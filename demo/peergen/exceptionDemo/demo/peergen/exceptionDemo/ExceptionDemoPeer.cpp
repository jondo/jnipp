#include "ExceptionDemoPeer.h"
#include "java/lang/UnsupportedOperationExceptionProxy.h"

using namespace demo::peergen::exceptionDemo;

ExceptionDemoPeer::ExceptionDemoPeer()
{
}

// methods
void ExceptionDemoPeer::throwAnException(JNIEnv* env, jobject obj)
{
	// TODO: Fill in your implementation here
	throw static_cast<jthrowable>(java::lang::UnsupportedOperationExceptionProxy());
}

