package demo.peergen.tutorial;

public interface NetAdapterServices
{
	public String[] getInterfaceList();
	public String getMACAddress(String interfaceName);
}