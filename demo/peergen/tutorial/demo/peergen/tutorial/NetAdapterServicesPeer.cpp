#include "NetAdapterServicesPeer.h"
#include "net/sourceforge/jnipp/JStringHelperArray.h"
#include "net/sourceforge/jnipp/JStringHelper.h"

#ifdef WIN32
#include <windows.h>
#include <nb30.h>
#else
#include <net/if.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#endif

using namespace demo::peergen::tutorial;
using namespace net::sourceforge::jnipp;

NetAdapterServicesPeer::NetAdapterServicesPeer()
{
}

// methods
jstring NetAdapterServicesPeer::getMACAddress(JNIEnv* env, jobject obj, jstring p0)
{
	// TODO: Fill in your implementation here
	JStringHelper interfaceName( p0 );
	JStringHelper retVal( "" );
	std::map<std::string, std::string>::iterator it = interfaceMap.find( static_cast<const char*>(interfaceName) );
	if ( it != interfaceMap.end() )
		retVal = it->second.c_str();

	return retVal;
}

jobjectArray NetAdapterServicesPeer::getInterfaceList(JNIEnv* env, jobject obj)
{
	// TODO: Fill in your implementation here
	JStringHelperArray retVal;
#ifdef WIN32
	struct ASTAT
	{
		ADAPTER_STATUS adapt;
		NAME_BUFFER	NameBuff[30];
	};

	NCB Ncb;
	LANA_ENUM lenum;
	ASTAT Adapter;

	// Get the list of Adapter numbers on this machine.	
	memset( &Ncb, 0, sizeof(Ncb) );
    Ncb.ncb_command = NCBENUM;
	Ncb.ncb_buffer = (UCHAR *)&lenum;
    Ncb.ncb_length = sizeof(lenum);

	UCHAR returnCode = Netbios( &Ncb );
	if ( (returnCode != NRC_GOODRET) || (lenum.length <= 0) )
		return retVal;

	for ( long index = 0; index < lenum.length; ++index )
	{
		// Reset the LAN Adapter
		ZeroMemory( &Ncb, sizeof(Ncb) );
		Ncb.ncb_command = NCBRESET;
		Ncb.ncb_lana_num = lenum.lana[index];
		returnCode = Netbios( &Ncb );

		if (returnCode != NRC_GOODRET)
			continue;

		// Get the adapter status
		ZeroMemory( &Ncb, sizeof(Ncb) );
		Ncb.ncb_command = NCBASTAT;
		Ncb.ncb_lana_num = lenum.lana[index];

		strcpy( reinterpret_cast<char*>( Ncb.ncb_callname ),  "*               " );
		Ncb.ncb_buffer = reinterpret_cast<unsigned char*>( &Adapter );
		Ncb.ncb_length = sizeof(Adapter);

		returnCode = Netbios( &Ncb );
		if ( returnCode != NRC_GOODRET )
			continue;

		char macAddress[13];
		sprintf( macAddress, 
				 "%02x%02x%02x%02x%02x%02x",
				 Adapter.adapt.adapter_address[0],
				 Adapter.adapt.adapter_address[1],
				 Adapter.adapt.adapter_address[2],
				 Adapter.adapt.adapter_address[3],
				 Adapter.adapt.adapter_address[4],
				 Adapter.adapt.adapter_address[5] );
		char name[10];
		sprintf( name, "%d", lenum.lana[index] );
		interfaceMap.insert( std::pair<std::string, std::string>( name, macAddress ) );
		retVal.addElement( JStringHelper( name ) );
	}

#else
	char macAddress[13];
	struct ifreq req;

	struct ifconf conf;
	struct ifreq interfaces[100];
	int sd = socket( PF_INET, SOCK_DGRAM, 0 );
	conf.ifc_len = sizeof( interfaces );
	conf.ifc_req = interfaces;

	if ( ioctl( sd, SIOCGIFCONF, &conf ) >= 0 )
		for ( int i = 0; i < conf.ifc_len/sizeof(struct ifreq); ++i )
		{
			unsigned char *hwaddr = reinterpret_cast<unsigned char*>( conf.ifc_req[i].ifr_hwaddr.sa_data );
			if ( ioctl( sd, SIOCGIFHWADDR, &conf.ifc_req[i] ) >= 0 )
			{
				sprintf( macAddress, "%02x%02x%02x%02x%02x%02x", hwaddr[0], hwaddr[1], hwaddr[2], hwaddr[3], hwaddr[4], hwaddr[5] );
				std::string interfaceName = conf.ifc_req[i].ifr_name;
				interfaceMap.insert( std::pair<std::string, std::string>( interfaceName, macAddress ) );
				retVal.addElement( JStringHelper( interfaceName.c_str() ) );
			}
		}
#endif

	return retVal;

}

