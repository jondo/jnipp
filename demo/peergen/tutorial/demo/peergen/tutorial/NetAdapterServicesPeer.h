#ifndef __demo_peergen_tutorial_NetAdapterServicesPeer_H
#define __demo_peergen_tutorial_NetAdapterServicesPeer_H

#ifdef WIN32
#pragma warning(disable:4251)
#pragma warning(disable:4786)
#endif

#include <jni.h>
#include <map>
#include <string>

namespace demo
{
	namespace peergen
	{
		namespace tutorial
		{
			class NetAdapterServicesPeer
			{
			private:
				std::map<std::string, std::string> interfaceMap;

			public:
				NetAdapterServicesPeer();

				// methods
				jstring getMACAddress(JNIEnv*, jobject, jstring);
				jobjectArray getInterfaceList(JNIEnv*, jobject);

			};
		};
	};
};


#endif
