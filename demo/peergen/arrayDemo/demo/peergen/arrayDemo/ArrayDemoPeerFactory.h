#ifndef __demo_peergen_arrayDemo_ArrayDemoPeerFactory_H
#define __demo_peergen_arrayDemo_ArrayDemoPeerFactory_H


#include "ArrayDemoPeer.h"
#include "ArrayDemoPeerImpl.h"

namespace demo
{
	namespace peergen
	{
		namespace arrayDemo
		{
			class ArrayDemoPeerFactory
			{
			public:
				static inline ArrayDemoPeer* newPeer()
				{
					/*
					 * TODO: Implement the factory method.  For example:
					 * return new ArrayDemoPeerImpl;
					 */
					return new ArrayDemoPeerImpl;
				}
			};
		};
	};
};


#endif
