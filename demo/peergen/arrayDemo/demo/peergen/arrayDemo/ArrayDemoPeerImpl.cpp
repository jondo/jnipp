#include "ArrayDemoPeerImpl.h"
#include "net/sourceforge/jnipp/JIntArrayHelper.h"
#include <iostream.h>

using namespace demo::peergen::arrayDemo;
using namespace net::sourceforge::jnipp;

// methods
jobjectArray ArrayDemoPeerImpl::getArray(JNIEnv* env, jobject obj)
{
	// TODO: Fill in your implementation here
	try
	{
		JIntArrayHelper<3> arr3( 3 );
		for ( int i = 0; i < 3; ++i )
		{
			JIntArrayHelper<2> arr2( 3 );
			for ( int j = 0; j < 3; ++j )
			{
				JIntArrayHelper<1> arr1( 3 );
				for ( int k = 0; k < 3; ++k )
					arr1.setElementAt( k, i*j*k );
				arr2.setElementAt( j, arr1 );
			}
			arr3.setElementAt( i, arr2 );
		}

		return arr3;
	}
	catch(jthrowable thr)
	{
		JNIEnvHelper::Throw( thr );
	}
}

void ArrayDemoPeerImpl::showArray(JNIEnv* env, jobject obj, jobjectArray p0)
{
	// TODO: Fill in your implementation here
	cout << "Contents of Java array:" << endl;
	JIntArrayHelper<3> arr3( p0 );
	int len3 = arr3.getLength();
	for ( int i = 0; i < len3; ++i )
	{
		JIntArrayHelper<2>& arr2 = arr3.getElementAt( i );
		int len2 = arr2.getLength();
		for ( int j = 0; j < len2; ++j )
		{
			JIntArrayHelper<1>& arr1 = arr2.getElementAt( j );
			int len1 = arr1.getLength();
			for ( int k = 0; k < len1; ++k )
				cout << arr1.getElementAt( k ) << " ";
			cout << endl;
		}
		cout << endl;
	}
}

