#ifndef __demo_peergen_arrayDemo_ArrayDemoPeerImpl_H
#define __demo_peergen_arrayDemo_ArrayDemoPeerImpl_H


#include <jni.h>
#include "ArrayDemoPeer.h"

namespace demo
{
	namespace peergen
	{
		namespace arrayDemo
		{
			class ArrayDemoPeerImpl : public ArrayDemoPeer
			{
			private:

			public:
				// methods
				jobjectArray getArray(JNIEnv*, jobject);
				void showArray(JNIEnv*, jobject, jobjectArray);

			};
		};
	};
};


#endif
