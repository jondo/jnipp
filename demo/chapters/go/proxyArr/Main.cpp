#include <iostream.h>
#include "net/sourceforge/jnipp/JVM.h"
#include "net/sourceforge/jnipp/JNIEnvHelper.h"
#include "net/sourceforge/jnipp/BaseException.h"
#include "net/sourceforge/jnipp/ProxyArray.h"
#include "net/sourceforge/jnipp/JStringHelper.h"
#include "java/io/FileProxy.h"

using namespace net::sourceforge::jnipp;
using namespace java::io;

int main(int argc, char** argv)
{
	if ( argc != 2 )
	{
		cerr << "Enter a directory name to list" << endl;
		return 1;
	}
	
	try
	{
		/*
		 * Note that we are relying on the JVM class to resolve the location of the JVM through the
		 * JVM_HOME environment variable.  An exception will be thrown if it is not set.
		 */
		JVM::load();
		
		JStringHelper path( argv[1] );
		::java::io::FileProxy myProxy( path );
#ifdef __NoPartialSpec
		net::sourceforge::jnipp::PA<java::io::FileProxy>::ProxyArray<1> fileList = myProxy.listFiles();
#else
		net::sourceforge::jnipp::ProxyArray<java::io::FileProxy, 1> fileList = myProxy.listFiles();
#endif
		
		cout << "Directory listing for " << JStringHelper( myProxy.getName() ) << ":" << endl;
		for ( int i = 0; i < fileList.getLength(); ++i )
		{
			FileProxy current = fileList.getElementAt( i );
			if ( current.isDirectory() == JNI_TRUE )
				cout << "[dir: " << JStringHelper( current.getName() ) << "]" << endl;
			else
				cout << JStringHelper( current.getName() ) << endl;
		}

		JVM::unload();
	}
	catch(BaseException& ex)
	{
		cerr << "caught exception: " << ex.getMessage().c_str() << endl;
	}

	return 0;
}
