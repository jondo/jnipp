#include <iostream.h>
#include "PrimArrDemoImpl.h"
#include "net/sourceforge/jnipp/JIntArrayHelper.h"

using namespace demo::chapters::go::primArr;
using namespace net::sourceforge::jnipp;

::net::sourceforge::jnipp::JIntArrayHelper<2> PrimArrDemoImpl::get2DArray(JNIEnv* env, jobject obj)
{
	JIntArrayHelper<2> retVal( 3 );
	for ( int i = 0; i < 3; ++i )
	{
		JIntArrayHelper<1> arr( 4 );
		for ( int j = 0; j < 4; ++j )
			arr.setElementAt( j, i*j );
		retVal.setElementAt( i, arr );
	}
	
	return retVal;
}

void PrimArrDemoImpl::show3DArray(JNIEnv* env, jobject obj, ::net::sourceforge::jnipp::JIntArrayHelper<3> p0)
{
	cout << "Contents of Java 3D array:" << endl;
	for ( int i = 0; i < p0.getLength(); ++i )
	{
		JIntArrayHelper<2> arr2D = p0.getElementAt( i );
		for ( int j = 0; j < arr2D.getLength(); ++j )
		{
			JIntArrayHelper<1> arr1D = arr2D.getElementAt( j );
			for ( int k = 0; k < arr1D.getLength(); ++k )
				cout << "\t" << arr1D.getElementAt( k );
			cout << endl;
		}
		cout << endl;
	}
}

