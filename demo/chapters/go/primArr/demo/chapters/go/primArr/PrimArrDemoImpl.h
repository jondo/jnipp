#ifndef __demo_chapters_go_primArr_PrimArrDemoImpl_H
#define __demo_chapters_go_primArr_PrimArrDemoImpl_H

#include "PrimArrDemoPeer.h"

namespace demo
{
	namespace chapters
	{
		namespace go
		{
			namespace primArr
			{
				class PrimArrDemoImpl : public PrimArrDemoPeer
				{
					::net::sourceforge::jnipp::JIntArrayHelper<2> get2DArray(JNIEnv* env, jobject obj);
					void show3DArray(JNIEnv* env, jobject obj, ::net::sourceforge::jnipp::JIntArrayHelper<3> p0);

				};
			};
		};
	};
};


#endif
