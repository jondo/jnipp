#ifndef __demo_chapters_go_primArr_PrimArrDemoPeerFactory_H
#define __demo_chapters_go_primArr_PrimArrDemoPeerFactory_H


#include "PrimArrDemoPeer.h"
#include "PrimArrDemoImpl.h"

namespace demo
{
	namespace chapters
	{
		namespace go
		{
			namespace primArr
			{
				class PrimArrDemoPeerFactory
				{
				public:
					static inline PrimArrDemoPeer* newPeer()
					{
						/*
						 * TODO: Implement the factory method.  For example:
						 * return new PrimArrDemoPeerImpl;
						 */
						return new PrimArrDemoImpl;
					}
				};
			};
		};
	};
};


#endif
