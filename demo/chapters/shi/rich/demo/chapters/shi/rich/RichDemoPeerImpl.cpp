#include "RichDemoPeerImpl.h"

#include "java/util/ArrayListProxy.h"
#include "net/sourceforge/jnipp/JStringHelperArray.h"
#include "java/lang/ObjectProxy.h"
#include "java/util/ListProxy.h"

using namespace demo::chapters::shi::rich;
using namespace java::util;
using namespace net::sourceforge::jnipp;
using namespace java::lang;

IteratorProxy RichDemoPeerImpl::getCollection(JNIEnv* env, jobject obj)
{
	ArrayListProxy alp;
	alp.add( ObjectProxy( JStringHelper( "I" ) ) );
	alp.add( ObjectProxy( JStringHelper( "love" ) ) );
	alp.add( ObjectProxy( JStringHelper( "JNI++" ) ) );
	
	ListProxy lp( alp );
	return lp.iterator();
}

