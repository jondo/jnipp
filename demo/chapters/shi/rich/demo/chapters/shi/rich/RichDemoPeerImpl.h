#ifndef __demo_chapters_shi_rich_RichDemoPeerImpl_H
#define __demo_chapters_shi_rich_RichDemoPeerImpl_H

#include "RichDemoPeer.h"

namespace demo
{
	namespace chapters
	{
		namespace shi
		{
			namespace rich
			{
				class RichDemoPeerImpl : public RichDemoPeer
				{
				public:
					::java::util::IteratorProxy getCollection(JNIEnv* env, jobject obj);
				};
			};
		};
	};
};


#endif
