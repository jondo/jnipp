#ifndef __demo_chapters_shi_rich_RichDemoPeerFactory_H
#define __demo_chapters_shi_rich_RichDemoPeerFactory_H


#include "RichDemoPeer.h"
#include "RichDemoPeerImpl.h"

namespace demo
{
	namespace chapters
	{
		namespace shi
		{
			namespace rich
			{
				class RichDemoPeerFactory
				{
				public:
					static inline RichDemoPeer* newPeer()
					{
						/*
						 * TODO: Implement the factory method.  For example:
						 * return new RichDemoPeerImpl;
						 */
						return new RichDemoPeerImpl;
					}
				};
			};
		};
	};
};


#endif
