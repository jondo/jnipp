#ifndef __demo_chapters_shi_derived_DerivedDemoPeerFactory_H
#define __demo_chapters_shi_derived_DerivedDemoPeerFactory_H


#include "DerivedDemoPeer.h"
#include "DerivedDemoPeerImpl.h"

namespace demo
{
	namespace chapters
	{
		namespace shi
		{
			namespace derived
			{
				class DerivedDemoPeerFactory
				{
				public:
					static inline DerivedDemoPeer* newPeer()
					{
						/*
						 * TODO: Implement the factory method.  For example:
						 * return new DerivedDemoPeerImpl;
						 */
						return new DerivedDemoPeerImpl;
					}
				};
			};
		};
	};
};


#endif
