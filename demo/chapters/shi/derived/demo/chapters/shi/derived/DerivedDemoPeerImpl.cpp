#include "DerivedDemoPeerImpl.h"
#include "net/sourceforge/jnipp/JStringHelper.h"
#include <iostream.h>

using namespace demo::chapters::shi::derived;
using namespace net::sourceforge::jnipp;

// methods
void DerivedDemoPeerImpl::printMessage(JNIEnv* env, jobject obj, jstring p0)
{
	cout << "The following message was printed from the C++ peer: " << JStringHelper( p0 ) << endl;
}

