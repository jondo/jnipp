#ifndef __demo_chapters_shi_derived_DerivedDemoPeerImpl_H
#define __demo_chapters_shi_derived_DerivedDemoPeerImpl_H

#include "DerivedDemoPeer.h"

namespace demo
{
	namespace chapters
	{
		namespace shi
		{
			namespace derived
			{
				class DerivedDemoPeerImpl : public DerivedDemoPeer
				{
				public:
					void printMessage(JNIEnv* env, jobject obj, jstring p0);
				};
			};
		};
	};
};

#endif
