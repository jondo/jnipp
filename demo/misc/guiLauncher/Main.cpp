#include <iostream.h>
#include "net/sourceforge/jnipp/JVM.h"
#include "net/sourceforge/jnipp/JNIEnvHelper.h"
#include "net/sourceforge/jnipp/JStringHelperArray.h"
#include "net/sourceforge/jnipp/pman/MainProxy.h"
#include "net/sourceforge/jnipp/BaseException.h"

using namespace net::sourceforge::jnipp;
using namespace net::sourceforge::jnipp::pman;

int main(int argc, char** argv)
{
	try
	{
		char* jnippHome = getenv( "JNIPP_HOME" );
		if ( jnippHome == NULL )
		{
			cerr << "Please set the JNIPP_HOME environment variable before launching." << endl;
			return 1;
		}
		
		if ( getenv( "JVM_HOME" ) == NULL )
		{
			cerr << "Please set the JVM_HOME environment variable before launching." << endl;
			return 2;
		}

		std::string jnippJar = jnippHome;
		std::string xmlJar = jnippHome;
		std::string xercesJar = jnippHome;
		jnippJar += "/lib/jnipp.jar";
		xmlJar += "/lib/xml-apis.jar";
		xercesJar += "/lib/xercesImpl.jar";
		JVM::appendClassPath( jnippJar );
		JVM::appendClassPath( xmlJar );
		JVM::appendClassPath( xercesJar );
		JVM::load();
		JStringHelperArray<1> args;
		for ( int i = 0; i < argc; ++i )
			args.addElement( JStringHelper( argv[i] ) );
		MainProxy::main( args );
		JVM::unload();
	}
	catch(BaseException& ex)
	{
		cerr << "caught exception: " << ex.getMessage().c_str() << endl;
	}

	return 0;
}
