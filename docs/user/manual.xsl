<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
		<fo:layout-master-set>
         <fo:simple-page-master master-name="cover-page" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1cm">
            <fo:region-body margin-top="3cm"/>
            <fo:region-before extent="3cm"/>
            <fo:region-after extent="1.5cm"/>
         </fo:simple-page-master>

         <fo:simple-page-master master-name="chapter-page" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1cm">
            <fo:region-body margin-top="3cm"/>
            <fo:region-before extent="3cm"/>
            <fo:region-after extent="1.5cm"/>
         </fo:simple-page-master>

			<fo:simple-page-master master-name="normal-page-left" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1cm">
				<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm"/>
				<fo:region-before extent="1cm" display-align="after" region-name="left-header"/>
				<fo:region-after extent="1cm" region-name="left-footer"/>
			</fo:simple-page-master>

			<fo:simple-page-master master-name="normal-page-right" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1cm">
				<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm"/>
				<fo:region-before extent="1cm" display-align="after" region-name="right-header"/>
				<fo:region-after extent="1cm" region-name="right-footer"/>
			</fo:simple-page-master>

			<fo:simple-page-master master-name="blank-page" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1cm">
				<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm" region-name="blank-body"/>
				<fo:region-before extent="1cm" display-align="after" region-name="right-header"/>
				<fo:region-after extent="1cm" region-name="right-footer"/>
			</fo:simple-page-master>

			<fo:page-sequence-master master-name="cover">
				<fo:single-page-master-reference master-reference="cover-page"/>
			</fo:page-sequence-master>
			
			<fo:page-sequence-master master-name="TOC">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference blank-or-not-blank="blank" master-reference="blank-page"/>
					<fo:conditional-page-master-reference odd-or-even="odd" master-reference="normal-page-right"/>
					<fo:conditional-page-master-reference odd-or-even="even" master-reference="normal-page-left"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>

			<fo:page-sequence-master master-name="body">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference blank-or-not-blank="blank" master-reference="blank-page"/>
					<fo:conditional-page-master-reference odd-or-even="odd" master-reference="normal-page-right"/>
					<fo:conditional-page-master-reference odd-or-even="even" master-reference="normal-page-left"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>
		</fo:layout-master-set>

		<xsl:apply-templates select="manual"/>
		<xsl:call-template name="toc"/>
		<xsl:call-template name="body"/>
	</fo:root>
</xsl:template>

<xsl:template match="manual">
   <fo:page-sequence master-reference="cover">
      <fo:static-content flow-name="xsl-region-before">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="3pt" leader-length.optimum="100%"/>
         </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="xsl-region-after">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
         <fo:block font-family="Helvetica" font-size="36pt" text-align="end" line-height="24pt" break-after="even-page">
            <xsl:value-of select="@title"/>
         </fo:block>
      </fo:flow>
   </fo:page-sequence> 
</xsl:template>

<xsl:template name="body">
	<fo:page-sequence master-reference="body" initial-page-number="1">
		<fo:static-content flow-name="left-header">
			<fo:block text-align="end" font-family="Times" font-size="9pt" font-weight="bold">
				<xsl:value-of select="//manual/@title"/>
			</fo:block>
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="right-header">
			<fo:block text-align="start" font-family="Times" font-size="9pt" font-weight="bold">
				<xsl:value-of select="//manual/@title"/>
			</fo:block>
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="left-footer">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
			<fo:block text-align="start" font-family="Times" font-size="9pt" font-weight="bold">
				Page <fo:page-number/> of <fo:page-number-citation ref-id="terminator"/>
			</fo:block>
		</fo:static-content>

		<fo:static-content flow-name="right-footer">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
			<fo:block text-align="end" font-family="Times" font-size="9pt" font-weight="bold">
				Page <fo:page-number/> of <fo:page-number-citation ref-id="terminator"/>
			</fo:block>
		</fo:static-content>

		<fo:static-content flow-name="blank-body">
			<fo:block font-family="Helvetica" font-size="9pt" font-style="italic" text-align="center">
				This page is intentionally left blank.
			</fo:block>
		</fo:static-content>

		<fo:flow flow-name="xsl-region-body">
			<xsl:apply-templates select="manual/chapter"/>
			<fo:block id="terminator"/>
		</fo:flow>
	</fo:page-sequence>
</xsl:template>

<xsl:template match="chapter">
	<fo:block font-family="Helvetica" font-size="24pt" space-after.optimum="12pt" space-before.optimum="24pt" id="{generate-id()}" break-before="odd-page" text-align="end">
		<xsl:call-template name="bar">
			<xsl:with-param name="text">
				<xsl:text>Chapter </xsl:text>
				<xsl:number level="multiple" format="1 " count="chapter"/>
			</xsl:with-param>
		</xsl:call-template>
	</fo:block>
	
	<fo:block font-family="Helvetica" text-align="end" font-size="20pt" space-after.optimum="12pt" space-before.optimum="12pt">
		<xsl:value-of select="@title"/>
	</fo:block>
	
	<fo:block start-indent="1em" end-indent="1em">
		<xsl:apply-templates select="h1"/>
	</fo:block>
</xsl:template>

<xsl:template match="h1">
	<fo:block font-family="Helvetica" font-size="16pt" font-style="italic" space-after.optimum="12pt" space-before.optimum="24pt" id="{generate-id()}">
		<xsl:number level="multiple" format="1.1 " count="h1 | chapter"/>
		<xsl:value-of select="@title"/>
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template match="h2">
	<fo:block font-family="Helvetica" font-size="14pt" font-style="normal" space-after.optimum="12pt" space-before.optimum="18pt" id="{generate-id()}">
		<xsl:number level="multiple" format="1.1.1 " count="h2 | h1 | chapter"/>
		<fo:inline text-decoration="underline">
			<xsl:value-of select="@title"/>			
		</fo:inline>
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template match="h3">
	<fo:block font-family="Helvetica" font-size="12pt" font-style="normal" space-after.optimum="12pt" space-before.optimum="18pt" id="{generate-id()}">
		<xsl:number level="multiple" format="1.1.1.1 " count="h3 | h2 | h1 | chapter"/>
		<xsl:value-of select="@title"/>			
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template name="bar">
	<xsl:param name="text"/>
		<fo:block font-weight="bold" background-color="gray" color="white">
			<xsl:value-of select="$text"/>
		</fo:block>
</xsl:template>

<xsl:template match="chapter" mode="toc">
	<fo:block>
		<fo:table>
			<fo:table-column column-width="17.2cm"/>
			<fo:table-column column-width="0.3cm"/>
			<fo:table-body font-size="12pt" font-family="Helvetica">
				<fo:table-row line-height="12pt">
					<fo:table-cell>
						<fo:block text-align="start">
							<fo:basic-link internal-destination="{generate-id()}">
								<xsl:value-of select="@title"/>
							</fo:basic-link>
							<fo:leader leader-pattern="dots" leader-pattern-width="8pt" leader-alignment="reference-area"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="end">
							<fo:page-number-citation ref-id="{generate-id()}"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:block>
	
	<fo:block space-after.optimum="12pt">
		<xsl:for-each select="h1">
			<xsl:apply-templates select="." mode="toc"/>
		</xsl:for-each>
	</fo:block>
	
</xsl:template>

<xsl:template match="h1" mode="toc">
	<fo:block>
		<fo:table>
			<fo:table-column column-width="17.2cm"/>
			<fo:table-column column-width="0.3cm"/>
			<fo:table-body font-size="10pt" font-style="italic" font-family="Helvetica">
	
				<fo:table-row line-height="12pt">
					<fo:table-cell>
						<fo:block text-align="start" text-indent="12pt">
							<fo:basic-link internal-destination="{generate-id()}">
								<xsl:value-of select="@title"/>
							</fo:basic-link>
							<fo:leader leader-pattern="dots" leader-pattern-width="8pt" leader-alignment="reference-area"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="end">
							<fo:page-number-citation ref-id="{generate-id()}"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="h2" mode="toc"/>
	</fo:block>
</xsl:template>

<xsl:template match="h2" mode="toc">
	<fo:block>
		<fo:table>
			<fo:table-column column-width="17.2cm"/>
			<fo:table-column column-width="0.3cm"/>
			<fo:table-body font-size="10pt" font-family="Helvetica">
	
				<fo:table-row line-height="12pt">
					<fo:table-cell>
						<fo:block text-align="start" text-indent="18pt">
							<fo:basic-link internal-destination="{generate-id()}">
								<xsl:value-of select="@title"/>
							</fo:basic-link>
							<fo:leader leader-pattern="dots" leader-pattern-width="8pt" leader-alignment="reference-area"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="end">
							<fo:page-number-citation ref-id="{generate-id()}"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="h3" mode="toc"/>
	</fo:block>
</xsl:template>

<xsl:template match="h3" mode="toc">
	<fo:block>
		<fo:table>
			<fo:table-column column-width="17.2cm"/>
			<fo:table-column column-width="0.3cm"/>
			<fo:table-body font-size="8pt" font-family="Helvetica">
	
				<fo:table-row line-height="12pt">
					<fo:table-cell>
						<fo:block text-align="start" text-indent="24pt">
							<fo:basic-link internal-destination="{generate-id()}">
								<xsl:value-of select="@title"/>
							</fo:basic-link>
							<fo:leader leader-pattern="dots" leader-pattern-width="8pt" leader-alignment="reference-area"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block text-align="end">
							<fo:page-number-citation ref-id="{generate-id()}"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:block>
</xsl:template>

<xsl:template name="toc">
	<fo:page-sequence master-reference="TOC" format="i" initial-page-number="1">
		<fo:static-content flow-name="left-header">
			<fo:block text-align="end" font-family="Times" font-size="9pt" font-weight="bold">
				<xsl:value-of select="//manual/@title"/>
			</fo:block>
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="right-header">
			<fo:block text-align="start" font-family="Times" font-size="9pt" font-weight="bold">
				<xsl:value-of select="//manual/@title"/>
			</fo:block>
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="left-footer">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
			<fo:block text-align="start" font-family="Times" font-size="9pt" font-weight="bold">
				<fo:page-number/>
			</fo:block>
		</fo:static-content>

		<fo:static-content flow-name="right-footer">
         <fo:block>
            <fo:leader leader-pattern="rule" rule-thickness="1pt" leader-length.optimum="100%"/>
         </fo:block>
			<fo:block text-align="end" font-family="Times" font-size="9pt" font-weight="bold">
				<fo:page-number/>
			</fo:block>
		</fo:static-content>

		<fo:static-content flow-name="blank-body">
			<fo:block font-family="Helvetica" font-size="9pt" font-style="italic" text-align="center">
				This page is intentionally left blank.
			</fo:block>
		</fo:static-content>

		<fo:flow flow-name="xsl-region-body" font-family="Helvetica">
			<fo:block font-size="24pt" space-before.optimum="30pt" space-after.optimum="24pt" text-align="start">
				<xsl:call-template name="bar">
					<xsl:with-param name="text">
						<xsl:text>Table of Contents</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
			</fo:block>

			<fo:block break-after="even-page">
				<xsl:for-each select="//chapter">
					<xsl:apply-templates select="." mode="toc"/>
				</xsl:for-each>
			</fo:block>
		</fo:flow>
	</fo:page-sequence>
</xsl:template>

<xsl:template match="codesnip">
	<fo:block padding-start="6pt" padding-end="6pt" border-style="solid" border-color="gray" border-width="0.5pt" border-before-width.conditionality="discard" border-after-width.conditionality="discard" space-before.optimum="12pt" space-after.optimum="6pt">
		<fo:list-block space-before.optimum="0pt" space-after.optimum="0pt" provisional-distance-between-starts="12pt" provisional-label-separation="0pt" font-size="8pt" font-family="Courier" font-style="italic">
      	<xsl:apply-templates/>
		</fo:list-block>  

		<xsl:if test="count(child::annotation) != 0">
			<fo:block font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="bold">
				<xsl:text>Notes:</xsl:text>
			</fo:block>
		
			<fo:list-block space-before.optimum="6pt" space-after.optimum="0pt" provisional-distance-between-starts="12pt" provisional-label-separation="0pt" font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="normal">
				<xsl:apply-templates mode="notes"/>
			</fo:list-block>
		</xsl:if>
	</fo:block>

	<fo:block text-align="center" font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="bold" space-after.optimum="6pt">
		<xsl:text> Code Snip </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="codesnip" from="chapter"/>
		<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
	</fo:block>
</xsl:template>

<xsl:template match="codesnip/text()">
	<fo:list-item space-before.optimum="0pt" space-after.optimum="0pt" padding-left="2pt"> 
		<fo:list-item-label end-indent="label-end()">
			<fo:block>
      	</fo:block>
		</fo:list-item-label>
		<fo:list-item-body start-indent="body-start()">
      	<fo:block white-space-collapse="false" text-align="start">
     			<xsl:value-of select="."/>
      	</fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="annotation">
	<fo:list-item space-before.optimum="0pt" space-after.optimum="0pt" padding-left="2pt"> 
		<fo:list-item-label end-indent="label-end()">
			<fo:block>
				<xsl:value-of select="count(preceding-sibling::annotation)+1"/><xsl:text>:</xsl:text>
      	</fo:block>
		</fo:list-item-label>
		<fo:list-item-body start-indent="body-start()">
      	<fo:block white-space-collapse="false" text-align="start" background-color="#E0E0E0" font-weight="bold">
     			<xsl:apply-templates select="text()"/>
      	</fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="annotation" mode="notes">
	<fo:list-item space-before.optimum="2pt" space-after.optimum="2pt">
		<fo:list-item-label end-indent="label-end()">
			<fo:block>
				<xsl:value-of select="count(preceding-sibling::annotation)+1"/><xsl:text>.</xsl:text>
      	</fo:block>
		</fo:list-item-label>
		<fo:list-item-body start-indent="body-start()">
			<fo:block language="en" country="US" hyphenate="true">
				<xsl:apply-templates select="note" mode="notes"/>
			</fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="annotation/note" mode="notes">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="output">
   <fo:block padding-start="6pt" padding-end="6pt" border-style="solid" background-color="#E0E0E0" color="black" border-width="0.5pt" border-color="black" text-align="start" white-space-collapse="false" space-before.optimum="6pt" space-after.optimum="6pt" keep-together.within-page="always" keep-together.within-column="always" hyphenate="false">
      <fo:block font-family="Courier" font-size="8pt" font-weight="bold" space-after.optimum="6pt" space-before.optimum="6pt" language="en" country="US">
			<xsl:value-of select="."/>
      </fo:block>
	</fo:block>
	<fo:block text-align="center" font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="bold" space-after.optimum="12pt">
		<xsl:text> Output Sample </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="output" from="chapter"/>
		<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
	</fo:block>
</xsl:template>

<xsl:template match="figure">
	<fo:block text-align="center" font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="bold" space-after.optimum="12pt">
		<fo:external-graphic>
			<xsl:attribute name="src"><xsl:text>file:</xsl:text><xsl:value-of select="@fileName"/></xsl:attribute>
		</fo:external-graphic>
	</fo:block>
	<fo:block text-align="center" font-size="8pt" font-family="Helvetica" font-style="normal" font-weight="bold" space-after.optimum="12pt">
		<xsl:text> Figure </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="figure" from="chapter"/>
		<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
	</fo:block>
</xsl:template>

<xsl:template match="ulist | olist">
	<xsl:variable name="list-type" select="name()"/>
	<xsl:variable name="list-level" select="count(ancestor-or-self::*[name()=$list-type])"/>
	<fo:list-block space-before.optimum="12pt" space-after.optimum="12pt">
		<xsl:apply-templates>
      	<xsl:with-param name="list-level" select="$list-level"/>
		</xsl:apply-templates>
	</fo:list-block>
</xsl:template>

<xsl:template match="ulist/item">
	<xsl:param name="list-level"/>
	<fo:list-item>
		<fo:list-item-label end-indent="label-end()">
			<fo:block text-align="start">
				<xsl:choose>
					<xsl:when test="($list-level mod 2) = 1">
						<fo:block>
							<fo:inline font-family="Symbol">&#183;</fo:inline>
						</fo:block>
					</xsl:when>

					<xsl:otherwise>
            		<xsl:text>-</xsl:text>
					</xsl:otherwise>
				</xsl:choose>  
			</fo:block>
		</fo:list-item-label>

		<fo:list-item-body start-indent="body-start()">
			<fo:block>
				<xsl:apply-templates/>
			</fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="olist/item">
	<xsl:param name="list-level"/>
	<fo:list-item>
		<fo:list-item-label end-indent="label-end()">
			<fo:block text-align="start">
				<xsl:choose>
					<xsl:when test="($list-level mod 2) = 1">
            		<xsl:number format="1."/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:number format="a."/>
					</xsl:otherwise>
				</xsl:choose> 
			</fo:block>
		</fo:list-item-label>

		<fo:list-item-body start-indent="body-start()">
			<fo:block>
				<xsl:apply-templates/>
			</fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="p">
	<fo:block text-align="justify" font-family="Helvetica" font-size="10pt" font-weight="normal" font-style="normal" space-after.optimum="6pt" space-before.optimum="6pt" language="en" country="US" hyphenate="true">
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template match="code">
   <fo:inline font-family="Courier">
      <xsl:value-of select="."/>
   </fo:inline>
</xsl:template>

<xsl:template match="emph">
	<fo:inline font-style="italic">
		<xsl:value-of select="."/>
	</fo:inline>
</xsl:template>

<xsl:template match="under">
	<fo:inline text-decoration="underline">
		<xsl:value-of select="."/>
	</fo:inline>
</xsl:template>

<xsl:template match="a">
	<fo:basic-link color="blue">
		<xsl:attribute name="external-destination"><xsl:value-of select="@href"/></xsl:attribute>
		<xsl:apply-templates/>
	</fo:basic-link>
</xsl:template>

<xsl:template match="table">
	<fo:block space-after.optimum="12pt" space-before.optimum="12pt" font-style="normal" font-family="Times" font-size="9pt" font-weight="normal">
		<fo:table>
			<xsl:for-each select="tc">
				<xsl:apply-templates select="." mode="columnspec"/>
			</xsl:for-each>
			<fo:table-header>
				<fo:table-row>
				<xsl:for-each select="tc">
					<xsl:apply-templates select="." mode="columnames"/>
				</xsl:for-each>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="header/tc">
					<xsl:apply-templates select="." mode="columnspec"/>
				</xsl:for-each>
				<xsl:for-each select="tr">
					<xsl:apply-templates select="."/>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</fo:block>
</xsl:template>

<xsl:template match="table/tc" mode="columnspec">
	<fo:table-column>
		<xsl:attribute name="column-width"><xsl:value-of select="@width"/></xsl:attribute>
	</fo:table-column>
</xsl:template>

<xsl:template match="table/tc" mode="columnames">
	<fo:table-cell border-style="solid" border-width="0.1mm" background-color="#D9D9D9">
		<fo:block space-after.optimum="1pt" space-before.optimum="1pt" font-weight="bold">
			<xsl:apply-templates/>
		</fo:block>
	</fo:table-cell>
</xsl:template>

<xsl:template match="table/tr">
	<fo:table-row>
		<xsl:apply-templates/>
	</fo:table-row>
</xsl:template>

<xsl:template match="table/tr/td">
	<fo:table-cell border-top-style="solid" border-style="solid" border-width="0.1mm">
		<fo:block space-after.optimum="1pt" space-before.optimum="1pt">
			<xsl:apply-templates/>
		</fo:block>
	</fo:table-cell>
</xsl:template>

</xsl:stylesheet>
