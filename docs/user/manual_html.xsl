<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="ISO-8859-1" indent="yes"/>

<xsl:template match="manual">
	<html>
	<head>
	<title><xsl:value-of select="@title"/></title>
	<style>
		Body
		{
			background-color: #ffffff;
			font-family: Helvetica;
			margin-left: 1cm;
			margin-right: 1cm;
			text-align: justify;
		}
		font.chaptertitle
		{
			font-weight: normal;
		}
		h3 font.sectiontitle
		{
			font-style: italic;
			font-weight: normal;
		}
		h4 font.sectiontitle
		{
			font-weight: normal;
			text-decoration: underline;
		}
		table.codesnip
		{
			border-width: thin;
			border-style: solid;
			border-color: #000000;
			width: 100%;
			border-spacing: 0;
			border-collapse: collapse;
		}
		table.codesnip tr td
		{
			text-align: left;
			vertical-align: top;
		}
		table.codesnip tr td code
		{
			font-size: 9pt;
			font-style: italic;
		}
		table.codesnip tr td.highlightcode
		{
			background-color: #cccccc;
		}
		table.codesnip tr td.number
		{
			width: 10px;
			font-style: normal;
		}
		table.codesnip tr td.notes
		{
			padding-left: .2cm;
		}
		table.codesnip tr td.notes code
		{
			font-style: normal;
		}
		table.codesnip tr td.caption
		{
			font-weight: bold;
			font-size: 80%;
			text-align: center;
		}		
		table.output
		{
			border-width: thin;
			border-style: solid;
			border-color: #000000;
			width: 100%;
			border-spacing: 0;
			border-collapse: collapse;
		}
		table.output tr td
		{
			font-size: 9pt;
		}
		table.output tr td.output
		{
			background-color: #333333;
			color: #ffffff;
			text-align: left;
			padding-left: .25cm;
			padding-top: .25cm;
			padding-bottom: .25cm;
		}
		table.output tr td.caption
		{
			font-weight: bold;
			font-size: 80%;
			text-align: center;
		}
		table.figure tr td
		{
			font-size: 9pt;
		}
		table.figure tr td.output
		{
			background-color: #333333;
			color: #ffffff;
			text-align: left;
			padding-left: .25cm;
			padding-top: .25cm;
			padding-bottom: .25cm;
		}
		table.figure tr td.caption
		{
			font-weight: bold;
			font-size: 80%;
			text-align: center;
		}
	</style>
	</head>
	<body>
	<h1><xsl:value-of select="@title"/></h1>
	<!-- <xsl:call-template name="toc"/> -->
	<xsl:apply-templates/>
	</body>
	</html>
</xsl:template>

<xsl:template match="chapter">
	<h2>
		<xsl:text>Chapter </xsl:text>
		<xsl:number level="multiple" format="1 " count="chapter"/>
	<font class="chaptertitle">
		<xsl:value-of select="@title"/>
	</font>
	</h2>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="h1">
	<h3>
	<xsl:number level="multiple" format="1.1 " count="h1 | chapter"/>
	<font class="sectiontitle">
	<xsl:value-of select="@title"/>
	</font>
	</h3>
	<p>
	<xsl:apply-templates/>
	</p>
</xsl:template>

<xsl:template match="h2">
	<h4>
	<xsl:number level="multiple" format="1.1.1 " count="h2 | h1 | chapter"/>
	<font class="sectiontitle">
	<xsl:value-of select="@title"/>
	</font>
	</h4>
	<p>
	<xsl:apply-templates/>
	</p>
</xsl:template>

<xsl:template match="codesnip">
	<p>
	<table class="codesnip">
		<tr>
		<td class="number">
		</td>
		<td>
			<code>
			<pre>
			<xsl:apply-templates/>
			</pre>
			</code>
		</td>
	</tr>
	<xsl:if test="count(child::annotation) != 0">
	<tr>
		<td colspan="2" class="notes">
			<code><b><xsl:text>Notes:</xsl:text></b></code>
			<code><xsl:apply-templates mode="notes"/></code>
		</td>
	</tr>
	</xsl:if>
	<tr>
		<td colspan="2" class="caption">
		<xsl:text> Code Snip </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="codesnip" from="chapter"/>
		<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
		</td>
	</tr>
	</table>
	</p>
</xsl:template>

<xsl:template match="codesnip/annotation">
	<xsl:text disable-output-escaping="yes">&lt;/pre&gt;&lt;/code&gt;&lt;/td&gt;&lt;/tr&gt;</xsl:text>
	<tr>
		<td class="number">
		<code><xsl:value-of select="count(preceding-sibling::annotation)+1"/><xsl:text>:</xsl:text></code>
		</td>
		<td class="highlightcode">
		<code><pre><xsl:apply-templates select="text()"/></pre></code>
		</td>
	</tr>
	<xsl:text disable-output-escaping="yes">&lt;tr&gt;&lt;td class="number"&gt;&lt;/td&gt;&lt;td&gt;&lt;code&gt;&lt;pre&gt;</xsl:text>
</xsl:template>

<xsl:template match="codesnip/text()" mode="notes">
</xsl:template>

<xsl:template match="annotation" mode="notes">
	<p>
	<xsl:value-of select="count(preceding-sibling::annotation)+1"/><xsl:text>.</xsl:text>
	<xsl:apply-templates select="note" mode="notes"/>
	</p>
</xsl:template>

<xsl:template match="note" mode="notes">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="output">
	<p>
	<table class="output">
		<tr>
			<td class="output">
				<code><pre><xsl:value-of select="."/></pre></code>
			</td>
		</tr>
		<tr>
			<td class="caption">
				<xsl:text> Output Sample </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="output" from="chapter"/>
				<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
			</td>
		</tr>
	</table>
	</p>
</xsl:template>

<xsl:template match="figure">
	<p>
	<table class="figure">
		<tr>
			<td class="figure">
				<img>
					<xsl:attribute name="src"><xsl:value-of select="@fileName"/></xsl:attribute>
				</img>
			</td>
		</tr>
		<tr>
			<td class="caption">
				<xsl:text> Figure </xsl:text> <xsl:number format="1" level="multiple" count="chapter"/> <xsl:text> - </xsl:text> <xsl:number format="1" level="any" count="figure" from="chapter"/>
				<xsl:text>: </xsl:text> <xsl:value-of select="@caption"/>
			</td>
		</tr>
	</table>
	</p>
</xsl:template>

<xsl:template match="ulist">
	<ul>
	<xsl:apply-templates/>
	</ul>
</xsl:template>

<xsl:template match="olist">
	<ol>
	<xsl:apply-templates/>
	</ol>
</xsl:template>

<xsl:template match="ulist | olist/item">
	<li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="p">
	<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="emph">
	<i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="under">
	<font style="text-decoration: underline;"><xsl:apply-templates/></font>
</xsl:template>

</xsl:stylesheet>
