package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.event.ActionEvent;

class ExitAction
      extends AbstractAction {
	private static ExitAction exitAction = new ExitAction();

	protected static ExitAction getExitAction() {
		return exitAction;
	}

	public ExitAction() {
		super("Exit", new ImageIcon(ExitAction.class.getResource("/net/sourceforge/jnipp/pman/images/Door.gif")));
		setEnabled(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getMainFrame().isDirty() == true) {
			Object[] options = {"Save", "Abandon", "Cancel"};
			int selection = JOptionPane.showOptionDialog(MainFrame.getMainFrame(),
			                                             "Project \"" + MainFrame.getMainFrame().getProject().getName() + "\" has been modified.  Save changes?",
			                                             "Project Modified",
			                                             JOptionPane.DEFAULT_OPTION,
			                                             JOptionPane.WARNING_MESSAGE,
			                                             null,
			                                             options,
			                                             options[0]);
			if (selection == 0)
				SaveProjectAction.getSaveProjectAction().actionPerformed(e);
			else if (selection == 2)
				return;
		}

		Main.exit();
	}
}


