package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.GNUMakefileSettings;
import net.sourceforge.jnipp.project.NMakefileSettings;
import net.sourceforge.jnipp.project.Project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

public class ProjectDetailsPane
      extends JPanel {
	private Project proj = null;
	private JComboBox targetType = new JComboBox(new Object[]{"Shared Library", "Executable"});
	private JCheckBox usePartialSpec = new JCheckBox("Use Partial Specialization");
	private JTextField projectNameField = new JTextField(10);
	private JTextField targetNameField = new JTextField(10);
	private JTextField cppOutputDirField = new JTextField(10);
	private JTextField javaOutputDirField = new JTextField(10);
	private JCheckBox generateNMakefile = new JCheckBox("Generate NMakefile");
	private JCheckBox generateGNUMakefile = new JCheckBox("Generate GNUMakefile");
	private JTextField nMakefileNameField = new JTextField(10);
	private JTextField gnuMakefileNameField = new JTextField(10);

	public ProjectDetailsPane() {
		super();
		setBorder(BorderFactory.createTitledBorder("Project Settings"));

		usePartialSpec.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proj.setUsePartialSpec(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		targetType.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            proj.setTargetType(targetType.getSelectedIndex() == 0 ? "shlib" : "exe");
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		projectNameField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            proj.setName(projectNameField.getText());
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		targetNameField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            proj.setTargetName(targetNameField.getText());
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		cppOutputDirField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            proj.setCPPOutputDir(cppOutputDirField.getText());
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		javaOutputDirField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            proj.setJavaOutputDir(javaOutputDirField.getText());
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		nMakefileNameField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            if (nMakefileNameField.getText().equals("") == true)
					            proj.setNMakefileSettings(null);
				            else
					            proj.setNMakefileSettings(new NMakefileSettings(proj, nMakefileNameField.getText()));
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		gnuMakefileNameField.addFocusListener
		      (
		            new FocusAdapter() {
			            public void focusLost(FocusEvent e) {
				            if (gnuMakefileNameField.getText().equals("") == true)
					            proj.setGNUMakefileSettings(null);
				            else
					            proj.setGNUMakefileSettings(new GNUMakefileSettings(proj, gnuMakefileNameField.getText()));
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		generateNMakefile.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            nMakefileNameField.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
				            if (e.getStateChange() != ItemEvent.SELECTED) {
					            proj.setNMakefileSettings(null);
					            nMakefileNameField.setText("");
					            super.itemStateChanged(e);
				            }
			            }
		            }
		      );

		generateGNUMakefile.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            gnuMakefileNameField.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
				            if (e.getStateChange() != ItemEvent.SELECTED) {
					            proj.setGNUMakefileSettings(null);
					            gnuMakefileNameField.setText("");
					            super.itemStateChanged(e);
				            }
			            }
		            }
		      );

		JPanel settingsPane = new JPanel();
		settingsPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JLabel projectNameLabel = new JLabel("Project Name:   ");
		JLabel targetNameLabel = new JLabel("Target Name:   ");
		JLabel targetTypeLabel = new JLabel("Target Type:   ");
		JLabel cppOutputDirLabel = new JLabel("C++ Output Directory:   ");
		JLabel javaOutputDirLabel = new JLabel("Java Output Directory:   ");
		JLabel nMakefileNameLabel = new JLabel("Name:   ");
		JLabel gnuMakefileNameLabel = new JLabel("Name:   ");

		GridBagLayout detailsLayout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(detailsLayout);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		detailsLayout.setConstraints(settingsPane, gbc);
		add(settingsPane);
		gbc.weighty = 0.0;

		GridBagLayout gbl = new GridBagLayout();
		settingsPane.setLayout(gbl);
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(projectNameLabel, gbc);
		settingsPane.add(projectNameLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(projectNameField, gbc);
		settingsPane.add(projectNameField);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(targetNameLabel, gbc);
		settingsPane.add(targetNameLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(targetNameField, gbc);
		settingsPane.add(targetNameField);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(targetTypeLabel, gbc);
		settingsPane.add(targetTypeLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(targetType, gbc);
		settingsPane.add(targetType);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(cppOutputDirLabel, gbc);
		settingsPane.add(cppOutputDirLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(cppOutputDirField, gbc);
		settingsPane.add(cppOutputDirField);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(javaOutputDirLabel, gbc);
		settingsPane.add(javaOutputDirLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(javaOutputDirField, gbc);
		settingsPane.add(javaOutputDirField);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		JLabel blankLabel = new JLabel();
		gbl.setConstraints(blankLabel, gbc);
		settingsPane.add(blankLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(usePartialSpec, gbc);
		settingsPane.add(usePartialSpec);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		blankLabel = new JLabel();
		gbl.setConstraints(blankLabel, gbc);
		settingsPane.add(blankLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(generateNMakefile, gbc);
		settingsPane.add(generateNMakefile);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(nMakefileNameLabel, gbc);
		settingsPane.add(nMakefileNameLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(nMakefileNameField, gbc);
		settingsPane.add(nMakefileNameField);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		blankLabel = new JLabel();
		gbl.setConstraints(blankLabel, gbc);
		settingsPane.add(blankLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(generateGNUMakefile, gbc);
		settingsPane.add(generateGNUMakefile);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbl.setConstraints(gnuMakefileNameLabel, gbc);
		settingsPane.add(gnuMakefileNameLabel);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(gnuMakefileNameField, gbc);
		settingsPane.add(gnuMakefileNameField);
	}

	public void setProject(Project proj) {
		this.proj = proj;
		projectNameField.setText(proj.getName() == null ? "" : proj.getName());
		targetNameField.setText(proj.getTargetName() == null ? "" : proj.getTargetName());
		cppOutputDirField.setText(proj.getCPPOutputDir() == null ? "" : proj.getCPPOutputDir());
		javaOutputDirField.setText(proj.getJavaOutputDir() == null ? "" : proj.getJavaOutputDir());
		targetType.setSelectedIndex(proj.getTargetType() == null ? 0 : proj.getTargetType().equals("shlib") == true ? 0 : 1);
		usePartialSpec.setSelected(proj.getUsePartialSpec());
		if (proj.getNMakefileSettings() != null) {
			generateNMakefile.setSelected(true);
			nMakefileNameField.setText(proj.getNMakefileSettings().getName());
		} else
			generateNMakefile.setSelected(false);

		if (proj.getGNUMakefileSettings() != null) {
			generateGNUMakefile.setSelected(true);
			gnuMakefileNameField.setText(proj.getGNUMakefileSettings().getName());
		} else
			generateGNUMakefile.setSelected(false);
	}
}
