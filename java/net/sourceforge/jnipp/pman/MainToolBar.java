package net.sourceforge.jnipp.pman;

import javax.swing.*;

public class MainToolBar
      extends JToolBar {
	public MainToolBar() {
		super();
		add(NewProjectAction.getNewProjectAction());
		add(OpenProjectAction.getOpenProjectAction());
		add(SaveProjectAction.getSaveProjectAction());
		add(SaveProjectAsAction.getSaveProjectAsAction());
		add(GenerateAction.getGenerateAction());
		add(ExitAction.getExitAction());
		addSeparator();
		add(CreateNewSectionAction.getCreateNewSectionAction());
		add(RemoveAllSectionsAction.getRemoveAllSectionsAction());
		add(DuplicateAction.getDuplicateAction());
	}
}

