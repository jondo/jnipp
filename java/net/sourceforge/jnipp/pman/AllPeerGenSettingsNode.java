package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.Project;

import javax.swing.tree.DefaultMutableTreeNode;

public class AllPeerGenSettingsNode
      extends DefaultMutableTreeNode {
	public AllPeerGenSettingsNode(Project proj) {
		setUserObject(proj);
	}

	public boolean getAllowsChildren() {
		return ((Project) getUserObject()).getPeerGenSettings().hasNext();
	}

	public boolean getIsLeaf() {
		return false;
	}

	public String toString() {
		return "Peer Generator Settings";
	}
}
