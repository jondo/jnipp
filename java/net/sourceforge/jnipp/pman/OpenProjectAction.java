package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.RecentProjectList;
import net.sourceforge.jnipp.project.Project;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

class OpenProjectAction
      extends AbstractAction {
	private static OpenProjectAction openProjectAction = new OpenProjectAction();

	protected static OpenProjectAction getOpenProjectAction() {
		return openProjectAction;
	}

	public OpenProjectAction() {
		super("Open ...", new ImageIcon(OpenProjectAction.class.getResource("/net/sourceforge/jnipp/pman/images/Open.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getMainFrame().isDirty() == true) {
			Object[] options = {"Save", "Abandon", "Cancel"};
			int selection = JOptionPane.showOptionDialog(MainFrame.getMainFrame(),
			                                             "Project \"" + MainFrame.getMainFrame().getProject().getName() + "\" has been modified.  Save changes?",
			                                             "Project Modified",
			                                             JOptionPane.DEFAULT_OPTION,
			                                             JOptionPane.WARNING_MESSAGE,
			                                             null,
			                                             options,
			                                             options[0]);
			if (selection == 0)
				SaveProjectAction.getSaveProjectAction().actionPerformed(e);
			else if (selection == 2)
				return;
		}

		String path = ".";
		if (MainFrame.getMainFrame().getFileName() != null)
			path = new File(MainFrame.getMainFrame().getFileName()).getParent();

		JFileChooser chooser = new JFileChooser(path);
		chooser.setFileFilter(new ProjectFileFilter());
		if (chooser.showOpenDialog(MainFrame.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			try {
				Project proj = new Project();
				proj.load(file.getAbsolutePath());
				MainFrame.getMainFrame().setProject(proj);
				MainFrame.getMainFrame().setFileName(file.getAbsolutePath());
				MainFrame.getMainFrame().recalculateEnabledActionStates();
				RecentProjectList.add(file.getAbsolutePath());
				MainFrame.getMainFrame().getMainMenuBar().rebuildRecentProjectMenu();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}


