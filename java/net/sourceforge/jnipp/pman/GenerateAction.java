package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.main.Main;

import javax.swing.*;
import java.awt.event.ActionEvent;

class GenerateAction
      extends AbstractAction {
	private static GenerateAction generateAction = new GenerateAction();

	protected static GenerateAction getGenerateAction() {
		return generateAction;
	}

	public GenerateAction() {
		super("Generate ...", new ImageIcon(GenerateAction.class.getResource("/net/sourceforge/jnipp/pman/images/ExecuteProject.gif")));
		setEnabled(true);
	}

	public void actionPerformed(ActionEvent e) {
		try {
			Main.generate(MainFrame.getMainFrame().getProject());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}


