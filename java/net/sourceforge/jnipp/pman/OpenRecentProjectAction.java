package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.RecentProject;
import net.sourceforge.jnipp.pman.util.RecentProjectList;
import net.sourceforge.jnipp.project.Project;

import javax.swing.*;
import java.awt.event.ActionEvent;

class OpenRecentProjectAction
      extends AbstractAction {
	private RecentProject recentProject = null;

	public OpenRecentProjectAction(RecentProject recentProject) {
		super(recentProject.getFullFileName());
		this.recentProject = recentProject;
	}

	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getMainFrame().isDirty() == true) {
			Object[] options = {"Save", "Abandon", "Cancel"};
			int selection = JOptionPane.showOptionDialog(MainFrame.getMainFrame(),
			                                             "Project \"" + MainFrame.getMainFrame().getProject().getName() + "\" has been modified.  Save changes?",
			                                             "Project Modified",
			                                             JOptionPane.DEFAULT_OPTION,
			                                             JOptionPane.WARNING_MESSAGE,
			                                             null,
			                                             options,
			                                             options[0]);
			if (selection == 0)
				SaveProjectAction.getSaveProjectAction().actionPerformed(e);
			else if (selection == 2)
				return;
		}
		try {
			Project proj = new Project();
			proj.load(recentProject.getFullFileName());
			RecentProjectList.add(recentProject.getFullFileName());
			MainFrame.getMainFrame().setProject(proj);
			MainFrame.getMainFrame().setFileName(recentProject.getFullFileName());
			MainFrame.getMainFrame().recalculateEnabledActionStates();
			MainFrame.getMainFrame().getMainMenuBar().rebuildRecentProjectMenu();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
