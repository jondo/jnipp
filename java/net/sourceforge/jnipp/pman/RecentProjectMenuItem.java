package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.RecentProject;

import javax.swing.*;

public class RecentProjectMenuItem
      extends JMenuItem {
	private RecentProject recentProject = null;

	public RecentProjectMenuItem(RecentProject recentProject, Action action) {
		super(action);
		this.recentProject = recentProject;
	}

	public void menuSelectionChanged(boolean isIncluded) {
		super.menuSelectionChanged(isIncluded);
		if (isIncluded == true)
			MainFrame.getMainFrame().setStatusText(recentProject.getFullFileName());
	}
}
