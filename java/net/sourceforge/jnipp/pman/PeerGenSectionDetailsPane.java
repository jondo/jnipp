package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.PeerGenSettings;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ItemEvent;

public class PeerGenSectionDetailsPane
      extends JPanel {
	private PeerGenSettings peerGenSettings = null;
	private JCheckBox destructive = new JCheckBox("Destructive");
	private JCheckBox richTypes = new JCheckBox("Rich Types");
	private JCheckBox inheritance = new JCheckBox("Inherited Implementation");
	private InputClassesPane inputClassesPane = new InputClassesPane(true);
	private ProxyGenSectionDetailsPane proxyPane = new ProxyGenSectionDetailsPane(true);

	public PeerGenSectionDetailsPane() {
		super();
		setLayout(new BorderLayout());
		setBorder(new TitledBorder("Peer Generator Settings"));
		JPanel settingsPane = new JPanel();
		settingsPane.setLayout(new BorderLayout());
		JPanel cbPane = new JPanel();
		cbPane.setLayout(new FlowLayout());
		cbPane.add(destructive);
		cbPane.add(richTypes);
		cbPane.add(inheritance);
		settingsPane.add(cbPane, BorderLayout.NORTH);
		settingsPane.add(proxyPane, BorderLayout.CENTER);
		proxyPane.setVisible(false);
		add(settingsPane, BorderLayout.NORTH);
		add(inputClassesPane, BorderLayout.CENTER);

		destructive.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            peerGenSettings.setDestructive(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		richTypes.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            peerGenSettings.setUseRichTypes(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		inheritance.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            peerGenSettings.setUseInheritance(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );
	}

	public void setPeerSettings(PeerGenSettings peerGenSettings) {
		this.peerGenSettings = peerGenSettings;
		destructive.setSelected(peerGenSettings.isDestructive());
		richTypes.setSelected(peerGenSettings.getUseRichTypes());
		inheritance.setSelected(peerGenSettings.getUseInheritance());
		inputClassesPane.setPeerSettings(peerGenSettings);

		if (peerGenSettings.getUseRichTypes() == true) {
			proxyPane.setProxySettings(peerGenSettings.getProxyGenSettings());
			proxyPane.setVisible(true);
		}
	}
}
