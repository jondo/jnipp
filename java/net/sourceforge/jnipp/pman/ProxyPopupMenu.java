package net.sourceforge.jnipp.pman;

import javax.swing.*;

public class ProxyPopupMenu
      extends JPopupMenu {
	public ProxyPopupMenu() {
		add(DuplicateAction.getDuplicateAction());
	}
}

