package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.RecentProjectList;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

class SaveProjectAsAction
      extends AbstractAction {
	private static SaveProjectAsAction saveProjectAsAction = new SaveProjectAsAction();

	protected static SaveProjectAsAction getSaveProjectAsAction() {
		return saveProjectAsAction;
	}

	public SaveProjectAsAction() {
		super("Save As ...", new ImageIcon(SaveProjectAsAction.class.getResource("/net/sourceforge/jnipp/pman/images/SaveAs.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		String path = ".";
		if (MainFrame.getMainFrame().getFileName() != null)
			path = new File(MainFrame.getMainFrame().getFileName()).getParent();

		JFileChooser chooser = new JFileChooser(path);

		chooser.setFileFilter(new ProjectFileFilter());
		if (chooser.showSaveDialog(MainFrame.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			try {
				MainFrame.getMainFrame().getProject().save(file.getAbsolutePath());
				MainFrame.getMainFrame().setFileName(file.getAbsolutePath());
				MainFrame.getMainFrame().recalculateEnabledActionStates();
				RecentProjectList.add(file.getAbsolutePath());
				MainFrame.getMainFrame().getMainMenuBar().rebuildRecentProjectMenu();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
