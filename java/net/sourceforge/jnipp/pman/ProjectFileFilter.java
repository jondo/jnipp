package net.sourceforge.jnipp.pman;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ProjectFileFilter
      extends FileFilter {
	public boolean accept(File file) {
		return file.isDirectory() || file.getName().endsWith(".xml");
	}

	public String getDescription() {
		return (".xml");
	}
}
