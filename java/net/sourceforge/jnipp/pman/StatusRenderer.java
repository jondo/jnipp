package net.sourceforge.jnipp.pman;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class StatusRenderer
      extends DefaultTreeCellRenderer {
	public Component getTreeCellRendererComponent(JTree tree,
	                                              Object value,
	                                              boolean selected,
	                                              boolean expanded,
	                                              boolean leaf,
	                                              int row,
	                                              boolean hasFocus) {
		Component comp = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

		if (value instanceof PeerGenSettingsNode)
			setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/settings.gif")));
		else if (value instanceof ProxyGenSettingsNode)
			setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/settings.gif")));
		else
			setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/" + (expanded == true ? "open" : "closed") + "Folder.gif")));

		return comp;
	}

}
