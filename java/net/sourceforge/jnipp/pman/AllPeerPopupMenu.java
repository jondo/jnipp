package net.sourceforge.jnipp.pman;

import javax.swing.*;

public class AllPeerPopupMenu
      extends JPopupMenu {
	public AllPeerPopupMenu() {
		add(CreateNewSectionAction.getCreateNewSectionAction());
		add(RemoveAllSectionsAction.getRemoveAllSectionsAction());
	}
}

