package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

public class ProxyGenSectionDetailsPane
      extends JPanel {
	private ProxyGenSettings proxyGenSettings = null;
	private JCheckBox attributeGetters = new JCheckBox("Generate Attribute Getters");
	private JCheckBox attributeSetters = new JCheckBox("Generate Attribute Setters");
	private JCheckBox inheritance = new JCheckBox("Generate Ancestry");
	private JCheckBox richTypes = new JCheckBox("Generate Rich Types");
	private JCheckBox innerClasses = new JCheckBox("Generate Inner Classes");
	private JTextField recursionLevel = new JTextField(2);
	private InputClassesPane inputClassesPane = new InputClassesPane(false);

	public ProxyGenSectionDetailsPane() {
		this(false);
	}

	public ProxyGenSectionDetailsPane(boolean peerChild) {
		super();
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder((peerChild == true ? "Rich Type Settings" : "Proxy Generator Settings")));
		JPanel settingsPane = new JPanel();
		settingsPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		settingsPane.setLayout(new GridLayout(0, 3));
		settingsPane.add(attributeGetters);
		settingsPane.add(attributeSetters);
		settingsPane.add(inheritance);
		settingsPane.add(richTypes);
		settingsPane.add(innerClasses);

		JPanel recursionLevelPane = new JPanel();
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		recursionLevelPane.setLayout(gbl);
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		JLabel lbl = new JLabel("Recursion Level:   ");
		gbl.setConstraints(lbl, gbc);
		recursionLevelPane.add(lbl);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbl.setConstraints(recursionLevel, gbc);
		recursionLevelPane.add(recursionLevel);

		settingsPane.add(new JLabel());
		settingsPane.add(recursionLevelPane);
		if (peerChild == false) {
			add(settingsPane, BorderLayout.NORTH);
			add(inputClassesPane, BorderLayout.CENTER);
		} else
			add(settingsPane, BorderLayout.CENTER);

		attributeGetters.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proxyGenSettings.setGenerateAttributeGetters(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		attributeSetters.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proxyGenSettings.setGenerateAttributeSetters(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		inheritance.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proxyGenSettings.setUseInheritance(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		richTypes.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proxyGenSettings.setUseRichTypes(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );

		innerClasses.addItemListener
		      (
		            new ItemAdapter() {
			            public void itemStateChanged(ItemEvent e) {
				            proxyGenSettings.setGenerateInnerClasses(e.getStateChange() == ItemEvent.SELECTED);
				            super.itemStateChanged(e);
			            }
		            }
		      );
	}

	public void setProxySettings(ProxyGenSettings proxyGenSettings) {
		this.proxyGenSettings = proxyGenSettings;
		attributeGetters.setSelected(proxyGenSettings.getGenerateAttributeGetters());
		attributeSetters.setSelected(proxyGenSettings.getGenerateAttributeSetters());
		inheritance.setSelected(proxyGenSettings.getUseInheritance());
		richTypes.setSelected(proxyGenSettings.getUseRichTypes());
		innerClasses.setSelected(proxyGenSettings.getGenerateInnerClasses());
		inputClassesPane.setProxySettings(proxyGenSettings);
	}
}
