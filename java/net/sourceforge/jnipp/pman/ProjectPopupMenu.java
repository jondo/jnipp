package net.sourceforge.jnipp.pman;

import javax.swing.*;

public class ProjectPopupMenu
      extends JPopupMenu {
	public ProjectPopupMenu() {
		add(NewProjectAction.getNewProjectAction());
		add(OpenProjectAction.getOpenProjectAction());
		add(SaveProjectAction.getSaveProjectAction());
		add(SaveProjectAsAction.getSaveProjectAsAction());
		add(RemoveAllSectionsAction.getRemoveAllSectionsAction());
		add(ExitAction.getExitAction());
	}
}

