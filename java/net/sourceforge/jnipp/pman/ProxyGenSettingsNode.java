package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.tree.DefaultMutableTreeNode;

public class ProxyGenSettingsNode
      extends DefaultMutableTreeNode {
	private static int nextSectionNumber = 0;
	private int sectionNumber = 0;

	protected static synchronized void resetSectionNumbers() {
		nextSectionNumber = 0;
	}

	private static synchronized int getNextSectionNumber() {
		return ++nextSectionNumber;
	}

	public ProxyGenSettingsNode(ProxyGenSettings proxyGenSettings) {
		setUserObject(proxyGenSettings);
		sectionNumber = getNextSectionNumber();
	}

	public boolean getAllowsChildren() {
		return false;
	}

	public boolean getIsLeaf() {
		return true;
	}

	public String toString() {
		return "Proxy Section " + sectionNumber;
	}
}
