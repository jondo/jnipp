package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

class SaveProjectAction
      extends AbstractAction {
	private static SaveProjectAction saveProjectAction = new SaveProjectAction();

	protected static SaveProjectAction getSaveProjectAction() {
		return saveProjectAction;
	}

	public SaveProjectAction() {
		super("Save", new ImageIcon(SaveProjectAction.class.getResource("/net/sourceforge/jnipp/pman/images/Save.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getMainFrame().getFileName() == null) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new ProjectFileFilter());
			if (chooser.showSaveDialog(MainFrame.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				try {
					MainFrame.getMainFrame().getProject().save(file.getAbsolutePath());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				MainFrame.getMainFrame().setFileName(file.getAbsolutePath());
				MainFrame.getMainFrame().recalculateEnabledActionStates();
			}
		} else {
			try {
				MainFrame.getMainFrame().getProject().save(MainFrame.getMainFrame().getFileName());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			MainFrame.getMainFrame().recalculateEnabledActionStates();
		}
	}
}
