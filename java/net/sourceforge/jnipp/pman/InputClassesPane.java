package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.classBrowser.ClassSelectionDialog;
import net.sourceforge.jnipp.pman.classBrowser.ClassesAddedEvent;
import net.sourceforge.jnipp.pman.classBrowser.ClassesAddedListener;
import net.sourceforge.jnipp.project.PeerGenSettings;
import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class InputClassesPane
      extends JPanel {
	private PeerGenSettings peerGenSettings = null;
	private ProxyGenSettings proxyGenSettings = null;
	private JList classList = new JList(new DefaultListModel());
	private JButton addButton = new JButton("Add ...");
	private JButton removeButton = new JButton("Remove");
	private JButton removeAllButton = new JButton("Remove All");
	private JPanel buttonPanel = new JPanel();
	private ClassSelectionDialog csd = null;
	private TitledBorder border = null;
	private boolean showInterfacesOnly = false;

	public InputClassesPane(boolean showInterfacesOnly) {
		super();
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder((showInterfacesOnly == true ? "Input Interfaces" : "Input Classes")));
		this.showInterfacesOnly = showInterfacesOnly;

		buttonPanel.setLayout(new GridLayout(1, 3));
		buttonPanel.add(addButton);
		buttonPanel.add(removeButton);
		buttonPanel.add(removeAllButton);

		removeButton.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            int[] selected = classList.getSelectedIndices();
				            DefaultListModel model = (DefaultListModel) classList.getModel();
				            for (int i = 0; i < selected.length; ++i) {
					            String current = (String) model.getElementAt(selected[i] - i);
					            model.removeElementAt(selected[i] - i);
					            if (peerGenSettings != null)
						            peerGenSettings.removeClassName(current);
					            else
						            proxyGenSettings.removeClassName(current);
				            }
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		removeAllButton.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            ((DefaultListModel) classList.getModel()).removeAllElements();
				            if (peerGenSettings != null)
					            peerGenSettings.removeAllClassNames();
				            else
					            proxyGenSettings.removeAllClassNames();
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );

		addButton.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            csd.show();
			            }
		            }
		      );

		JScrollPane sp = new JScrollPane(classList);
		add(sp, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		csd = new ClassSelectionDialog(MainFrame.getMainFrame(), "Add Classes");
		csd.addClassesAddedListener
		      (
		            new ClassesAddedListener() {
			            public void classesAdded(ClassesAddedEvent e) {
				            Iterator it = e.getClassNames();
				            while (it.hasNext() == true) {
					            String next = (String) it.next();
					            ((DefaultListModel) classList.getModel()).addElement(next);
					            if (peerGenSettings != null)
						            peerGenSettings.addClassName(next);
					            else
						            proxyGenSettings.addClassName(next);
				            }
				            MainFrame.getMainFrame().recalculateEnabledActionStates();
			            }
		            }
		      );
		csd.pack();
	}

	public void setPeerSettings(PeerGenSettings peerGenSettings) {
		proxyGenSettings = null;
		this.peerGenSettings = peerGenSettings;
		setInputClasses(peerGenSettings.getClassNames());
	}

	public void setProxySettings(ProxyGenSettings proxyGenSettings) {
		peerGenSettings = null;
		this.proxyGenSettings = proxyGenSettings;
		setInputClasses(proxyGenSettings.getClassNames());
	}

	private void setInputClasses(Iterator it) {
		((DefaultListModel) classList.getModel()).removeAllElements();
		while (it.hasNext() == true)
			((DefaultListModel) classList.getModel()).addElement(it.next());
	}
}
