package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.PeerGenSettings;
import net.sourceforge.jnipp.project.Project;
import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.*;
import java.awt.*;

public class DetailsPane
      extends JPanel {
	private Project proj = null;
	private ProjectDetailsPane projectPane = new ProjectDetailsPane();
	private PeerGenSectionDetailsPane peerPane = new PeerGenSectionDetailsPane();
	private ProxyGenSectionDetailsPane proxyPane = new ProxyGenSectionDetailsPane();
	private JPanel mainPanel = new JPanel();
	private CardLayout proxyPeerLayout = new CardLayout();


	private static DetailsPane detailsPane = new DetailsPane();

	protected static DetailsPane getDetailsPane() {
		return detailsPane;
	}

	public DetailsPane() {
		super();
		setLayout(new BorderLayout());
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(projectPane);
		mainPanel.add(peerPane);
		mainPanel.add(proxyPane);
		add(mainPanel, BorderLayout.CENTER);
		setPreferredSize(mainPanel.getPreferredSize());
		hideProxyPeerPanes();
	}

	protected void hideProxyPeerPanes() {
		proxyPane.setVisible(false);
		peerPane.setVisible(false);
	}

	protected void showProxyPane(ProxyGenSettings proxySettings) {
		proxyPane.setProxySettings(proxySettings);
		peerPane.setVisible(false);
		proxyPane.setVisible(true);
	}

	protected void showPeerPane(PeerGenSettings peerSettings) {
		peerPane.setPeerSettings(peerSettings);
		proxyPane.setVisible(false);
		peerPane.setVisible(true);
	}

	public void setProject(Project proj) {
		this.proj = proj;
		projectPane.setProject(proj);
	}
}
