package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.PeerGenSettings;
import net.sourceforge.jnipp.project.Project;
import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;

public class MainFrame
      extends JFrame {
	private Project proj = null;
	private Project originalState = null;
	private JTree tree = null;
	private DefaultMutableTreeNode currentlySelectedNode = null;
	private MainMenuBar menuBar = new MainMenuBar();
	private MainToolBar toolBar = new MainToolBar();
	private ProjectPopupMenu projectPopupMenu = new ProjectPopupMenu();
	private AllPeerPopupMenu allPeerPopupMenu = new AllPeerPopupMenu();
	private AllProxyPopupMenu allProxyPopupMenu = new AllProxyPopupMenu();
	private PeerPopupMenu peerPopupMenu = new PeerPopupMenu();
	private ProxyPopupMenu proxyPopupMenu = new ProxyPopupMenu();
	private static MainFrame mainFrame = null;
	private String fileName = null;
	private StatusPane statusPanel = new StatusPane();

	public MainFrame(Project proj) {
		super("JNI++ Project Manager");
		getContentPane().setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		getContentPane().add(statusPanel, BorderLayout.SOUTH);
		mainFrame = this;
		this.proj = proj;
		originalState = (Project) proj.clone();
		tree = new JTree(createTreeModel());
		tree.setShowsRootHandles(true);
		tree.setCellRenderer(new StatusRenderer());
		tree.addMouseListener
		      (
		            new MouseAdapter() {
			            public void mousePressed(MouseEvent e) {
				            showPopup(e);
			            }

			            public void mouseReleased(MouseEvent e) {
				            showPopup(e);
			            }

			            private void showPopup(MouseEvent e) {
				            if (e.isPopupTrigger() == true) {
					            DefaultMutableTreeNode node = getCurrentlySelectedNode();
					            if (node == null || node instanceof ProjectNode)
						            projectPopupMenu.show(e.getComponent(), e.getX(), e.getY());
					            else if (node != null && node instanceof AllPeerGenSettingsNode)
						            allPeerPopupMenu.show(e.getComponent(), e.getX(), e.getY());
					            else if (node != null && node instanceof AllProxyGenSettingsNode)
						            allProxyPopupMenu.show(e.getComponent(), e.getX(), e.getY());
					            else if (node != null && node instanceof PeerGenSettingsNode)
						            peerPopupMenu.show(e.getComponent(), e.getX(), e.getY());
					            else if (node != null && node instanceof ProxyGenSettingsNode)
						            proxyPopupMenu.show(e.getComponent(), e.getX(), e.getY());
				            }
			            }
		            }
		      );

		mainPanel.add(toolBar, BorderLayout.NORTH);
		DetailsPane.getDetailsPane().setProject(proj);
		JSplitPane topPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(tree), new JScrollPane(DetailsPane.getDetailsPane()));
		topPane.setResizeWeight(0.7);
		topPane.setOneTouchExpandable(true);
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPane, OutputPane.getOutputPane());
		splitPane.setResizeWeight(0.9);
		splitPane.setOneTouchExpandable(true);
		mainPanel.add(splitPane, BorderLayout.CENTER);

		tree.addTreeSelectionListener
		      (
		            new TreeSelectionListener() {
			            public void valueChanged(TreeSelectionEvent e) {
				            TreePath path = e.getNewLeadSelectionPath();

				            if (path == null)
					            return;

				            currentlySelectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
				            recalculateEnabledActionStates();
				            setVisibleDetails();
			            }
		            }
		      );

		tree.getModel().addTreeModelListener
		      (
		            new TreeModelListener() {
			            public void treeNodesChanged(TreeModelEvent e) {
				            recalculateEnabledActionStates();
			            }

			            public void treeNodesInserted(TreeModelEvent e) {
				            recalculateEnabledActionStates();
			            }

			            public void treeNodesRemoved(TreeModelEvent e) {
				            recalculateEnabledActionStates();
			            }

			            public void treeStructureChanged(TreeModelEvent e) {
				            recalculateEnabledActionStates();
			            }
		            }
		      );

		setJMenuBar(menuBar);
		recalculateEnabledActionStates();
	}

	private DefaultTreeModel createTreeModel() {
		DefaultTreeModel model = new DefaultTreeModel(createProjectBranch());
		model.setAsksAllowsChildren(true);
		return model;
	}

	private ProjectNode createProjectBranch() {
		ProjectNode projectNode = new ProjectNode(proj);
		projectNode.add(createProxyBranch());
		projectNode.add(createPeerBranch());
		return projectNode;
	}

	private AllProxyGenSettingsNode createProxyBranch() {
		AllProxyGenSettingsNode node = new AllProxyGenSettingsNode(proj);
		Iterator proxyGenSettings = proj.getProxyGenSettings();
		ProxyGenSettingsNode.resetSectionNumbers();
		while (proxyGenSettings.hasNext() == true)
			node.add(new ProxyGenSettingsNode((ProxyGenSettings) proxyGenSettings.next()));
		return node;
	}

	private AllPeerGenSettingsNode createPeerBranch() {
		AllPeerGenSettingsNode node = new AllPeerGenSettingsNode(proj);
		Iterator peerGenSettings = proj.getPeerGenSettings();
		PeerGenSettingsNode.resetSectionNumbers();
		while (peerGenSettings.hasNext() == true)
			node.add(new PeerGenSettingsNode((PeerGenSettings) peerGenSettings.next()));
		return node;
	}

	protected DefaultMutableTreeNode getCurrentlySelectedNode() {
		return currentlySelectedNode;
	}

	protected JTree getTree() {
		return tree;
	}

	protected MainMenuBar getMainMenuBar() {
		return menuBar;
	}

	protected Project getProject() {
		return proj;
	}

	protected void setProject(Project proj) {
		this.proj = proj;
		originalState = (Project) proj.clone();
		rebuildTree();
		DetailsPane.getDetailsPane().setProject(proj);
		currentlySelectedNode = null;
		recalculateEnabledActionStates();
		setVisibleDetails();
	}

	protected void setStatusText(String text) {
		statusPanel.setText(text);
	}

	protected static MainFrame getMainFrame() {
		return mainFrame;
	}

	protected String getFileName() {
		return fileName;
	}

	protected void setFileName(String fileName) {
		this.fileName = fileName;
	}

	protected boolean isDirty() {
		return proj.equals(originalState) == false;
	}

	protected void rebuildTree() {
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		model.reload();
		model.setRoot(createProjectBranch());
	}

	protected void recalculateEnabledActionStates() {
		SaveProjectAsAction.getSaveProjectAsAction().setEnabled(isDirty());
		if (fileName != null)
			SaveProjectAction.getSaveProjectAction().setEnabled(isDirty());
		else
			SaveProjectAction.getSaveProjectAction().setEnabled(false);

		if (currentlySelectedNode == null || currentlySelectedNode instanceof ProjectNode) {
			boolean enableRemove = getProject().getPeerGenSettings().hasNext() || getProject().getProxyGenSettings().hasNext();
			RemoveAllSectionsAction.getRemoveAllSectionsAction().setEnabled(enableRemove);
			CreateNewSectionAction.getCreateNewSectionAction().setEnabled(false);
			DuplicateAction.getDuplicateAction().setEnabled(false);
		} else if (currentlySelectedNode instanceof AllPeerGenSettingsNode) {
			RemoveAllSectionsAction.getRemoveAllSectionsAction().setEnabled(getProject().getPeerGenSettings().hasNext());
			CreateNewSectionAction.getCreateNewSectionAction().setEnabled(true);
			DuplicateAction.getDuplicateAction().setEnabled(false);
		} else if (currentlySelectedNode instanceof AllProxyGenSettingsNode) {
			RemoveAllSectionsAction.getRemoveAllSectionsAction().setEnabled(getProject().getProxyGenSettings().hasNext());
			CreateNewSectionAction.getCreateNewSectionAction().setEnabled(true);
			DuplicateAction.getDuplicateAction().setEnabled(false);
		} else if (currentlySelectedNode instanceof PeerGenSettingsNode) {
			RemoveAllSectionsAction.getRemoveAllSectionsAction().setEnabled(false);
			CreateNewSectionAction.getCreateNewSectionAction().setEnabled(false);
			DuplicateAction.getDuplicateAction().setEnabled(true);
		} else if (currentlySelectedNode instanceof ProxyGenSettingsNode) {
			RemoveAllSectionsAction.getRemoveAllSectionsAction().setEnabled(false);
			CreateNewSectionAction.getCreateNewSectionAction().setEnabled(false);
			DuplicateAction.getDuplicateAction().setEnabled(true);
		}
	}

	private void setVisibleDetails() {
		if (currentlySelectedNode instanceof PeerGenSettingsNode)
			DetailsPane.getDetailsPane().showPeerPane((PeerGenSettings) ((DefaultMutableTreeNode) currentlySelectedNode).getUserObject());
		else if (currentlySelectedNode instanceof ProxyGenSettingsNode)
			DetailsPane.getDetailsPane().showProxyPane((ProxyGenSettings) ((DefaultMutableTreeNode) currentlySelectedNode).getUserObject());
		else
			DetailsPane.getDetailsPane().hideProxyPeerPanes();
	}
}
