package net.sourceforge.jnipp.pman;

import javax.swing.*;

public class AllProxyPopupMenu
      extends JPopupMenu {
	public AllProxyPopupMenu() {
		add(CreateNewSectionAction.getCreateNewSectionAction());
		add(RemoveAllSectionsAction.getRemoveAllSectionsAction());
	}
}

