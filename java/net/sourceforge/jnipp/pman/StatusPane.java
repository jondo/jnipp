package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.*;

public class StatusPane
      extends JPanel {
	private JLabel statusLabel = new JLabel("Status text");

	public StatusPane() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setBorder(BorderFactory.createEtchedBorder());

		add(statusLabel);
	}

	public void setText(String text) {
		statusLabel.setText(text);
	}
}
