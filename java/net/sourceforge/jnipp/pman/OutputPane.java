package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.*;

public class OutputPane
      extends JPanel {
	private static OutputPane outputPane = new OutputPane();
	private JTextArea outputArea = new JTextArea();

	protected static OutputPane getOutputPane() {
		return outputPane;
	}

	public OutputPane() {
		super();
		setLayout(new BorderLayout());
		add(new JScrollPane(outputArea), BorderLayout.CENTER);
		setPreferredSize(outputArea.getPreferredSize());
	}

	public synchronized void appendText(String text) {
		outputArea.append(text + "\n");
	}
}
