package net.sourceforge.jnipp.pman;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.ActionEvent;

class RemoveSectionAction
      extends AbstractAction {
	private static RemoveSectionAction removeSectionAction = new RemoveSectionAction();

	protected static RemoveSectionAction getRemoveSectionAction() {
		return removeSectionAction;
	}

	public RemoveSectionAction() {
		super("Remove", new ImageIcon(RemoveAllSectionsAction.class.getResource("/net/sourceforge/jnipp/pman/images/Minus.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		DefaultMutableTreeNode currentlySelectedNode = MainFrame.getMainFrame().getCurrentlySelectedNode();

		if (currentlySelectedNode == null)
			return;

		if (currentlySelectedNode instanceof AllPeerGenSettingsNode) {
			MainFrame.getMainFrame().getProject().removeAllPeerGenSettings();
		} else {
			MainFrame.getMainFrame().getProject().removeAllProxyGenSettings();
		}
		((DefaultTreeModel) MainFrame.getMainFrame().getTree().getModel()).nodeStructureChanged(currentlySelectedNode);
	}
}


