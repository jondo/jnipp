package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.PeerGenSettings;
import net.sourceforge.jnipp.project.ProxyGenSettings;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.ActionEvent;

class CreateNewSectionAction
      extends AbstractAction {
	private static CreateNewSectionAction createNewSectionAction = new CreateNewSectionAction();

	protected static CreateNewSectionAction getCreateNewSectionAction() {
		return createNewSectionAction;
	}

	public CreateNewSectionAction() {
		super("New Section", new ImageIcon(CreateNewSectionAction.class.getResource("/net/sourceforge/jnipp/pman/images/Plus.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		DefaultMutableTreeNode currentlySelectedNode = MainFrame.getMainFrame().getCurrentlySelectedNode();

		if (currentlySelectedNode == null)
			return;

		if (currentlySelectedNode instanceof AllPeerGenSettingsNode) {
			PeerGenSettings peerGenSettings = new PeerGenSettings(MainFrame.getMainFrame().getProject());
			MainFrame.getMainFrame().getProject().addPeerGenSettings(peerGenSettings);
			currentlySelectedNode.add(new PeerGenSettingsNode(peerGenSettings));
		} else {
			ProxyGenSettings proxyGenSettings = new ProxyGenSettings(MainFrame.getMainFrame().getProject());
			MainFrame.getMainFrame().getProject().addProxyGenSettings(proxyGenSettings);
			currentlySelectedNode.add(new ProxyGenSettingsNode(proxyGenSettings));
		}

		((DefaultTreeModel) MainFrame.getMainFrame().getTree().getModel()).nodeStructureChanged(currentlySelectedNode);
	}
}


