package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.Project;

import javax.swing.tree.DefaultMutableTreeNode;

public class AllProxyGenSettingsNode
      extends DefaultMutableTreeNode {
	public AllProxyGenSettingsNode(Project proj) {
		setUserObject(proj);
	}

	public boolean getAllowsChildren() {
		return ((Project) getUserObject()).getProxyGenSettings().hasNext();
	}

	public boolean getIsLeaf() {
		return false;
	}

	public String toString() {
		return "Proxy Generator Settings";
	}
}
