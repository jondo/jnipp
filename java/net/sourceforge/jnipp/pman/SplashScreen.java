package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.*;

public class SplashScreen
      extends JWindow {
	private static SplashScreen splashScreen = null;

	private SplashScreen() {
		JLabel label = new JLabel();
		label.setIcon(new ImageIcon(SplashScreen.class.getResource("/net/sourceforge/jnipp/pman/images/jnippSplash.jpg")));
		getContentPane().add(label);
		pack();
		Dimension screenDims = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(screenDims.width / 2 - getSize().width / 2,
		            screenDims.height / 2 - getSize().height / 2);
	}

	protected static void showSplashScreen() {
		splashScreen = new SplashScreen();
		splashScreen.setVisible(true);
	}

	protected static void hideSplashScreen() {
		if (splashScreen != null) {
			splashScreen.setVisible(false);
			splashScreen = null;
		}
	}
}
