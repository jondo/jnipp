package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.PeerGenSettings;

import javax.swing.tree.DefaultMutableTreeNode;

public class PeerGenSettingsNode
      extends DefaultMutableTreeNode {
	private static int nextSectionNumber = 0;
	private int sectionNumber = 0;

	protected static synchronized void resetSectionNumbers() {
		nextSectionNumber = 0;
	}

	private static synchronized int getNextSectionNumber() {
		return ++nextSectionNumber;
	}

	public PeerGenSettingsNode(PeerGenSettings peerGenSettings) {
		setUserObject(peerGenSettings);
		sectionNumber = getNextSectionNumber();
	}

	public boolean getAllowsChildren() {
		return false;
	}

	public boolean getIsLeaf() {
		return true;
	}

	public String toString() {
		return "Peer Section " + sectionNumber;
	}
}
