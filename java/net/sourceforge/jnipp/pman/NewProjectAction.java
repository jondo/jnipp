package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.Project;

import javax.swing.*;
import java.awt.event.ActionEvent;

class NewProjectAction
      extends AbstractAction {
	private static NewProjectAction newProjectAction = new NewProjectAction();

	protected static NewProjectAction getNewProjectAction() {
		return newProjectAction;
	}

	public NewProjectAction() {
		super("New", new ImageIcon(NewProjectAction.class.getResource("/net/sourceforge/jnipp/pman/images/New.gif")));
	}

	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getMainFrame().isDirty() == true) {
			Object[] options = {"Save", "Abandon", "Cancel"};
			int selection = JOptionPane.showOptionDialog(MainFrame.getMainFrame(),
			                                             "Project \"" + MainFrame.getMainFrame().getProject().getName() + "\" has been modified.  Save changes?",
			                                             "Project Modified",
			                                             JOptionPane.DEFAULT_OPTION,
			                                             JOptionPane.WARNING_MESSAGE,
			                                             null,
			                                             options,
			                                             options[0]);
			if (selection == 0)
				SaveProjectAction.getSaveProjectAction().actionPerformed(e);
			else if (selection == 2)
				return;
		}

		Project newProject = new Project();
		newProject.setName("Untitled");
		MainFrame.getMainFrame().setProject(newProject);
	}
}


