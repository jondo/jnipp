package net.sourceforge.jnipp.pman;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ItemAdapter
      implements ItemListener {
	public void itemStateChanged(ItemEvent itemEvent) {
		MainFrame.getMainFrame().recalculateEnabledActionStates();
	}
}
