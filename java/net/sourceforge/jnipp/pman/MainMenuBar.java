package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.RecentProject;
import net.sourceforge.jnipp.pman.util.RecentProjectList;

import javax.swing.*;
import java.util.Iterator;

public class MainMenuBar
      extends JMenuBar {
	private JMenu recentMenu = null;

	public MainMenuBar() {
		super();

		JMenu fileMenu = new JMenu("Project");
		fileMenu.add(NewProjectAction.getNewProjectAction());
		recentMenu = new JMenu("Recent Projects");
		fileMenu.add(recentMenu);
		rebuildRecentProjectMenu();
		fileMenu.add(OpenProjectAction.getOpenProjectAction());
		fileMenu.add(SaveProjectAction.getSaveProjectAction());
		fileMenu.add(SaveProjectAsAction.getSaveProjectAsAction());
		fileMenu.add(GenerateAction.getGenerateAction());
		fileMenu.addSeparator();
		fileMenu.add(ExitAction.getExitAction());
		add(fileMenu);

		JMenu sectionMenu = new JMenu("Section");
		sectionMenu.add(CreateNewSectionAction.getCreateNewSectionAction());
		sectionMenu.add(RemoveAllSectionsAction.getRemoveAllSectionsAction());
		sectionMenu.add(DuplicateAction.getDuplicateAction());
		add(sectionMenu);
	}

	public void rebuildRecentProjectMenu() {
		recentMenu.removeAll();
		Iterator it = RecentProjectList.getRecentProjects();
		if (it.hasNext() == false)
			recentMenu.setEnabled(false);
		else {
			recentMenu.setEnabled(true);
			while (it.hasNext() == true) {
				RecentProject rp = (RecentProject) it.next();
				RecentProjectMenuItem next = new RecentProjectMenuItem(rp, new OpenRecentProjectAction(rp));
				recentMenu.add(next);
			}
		}
	}
}

