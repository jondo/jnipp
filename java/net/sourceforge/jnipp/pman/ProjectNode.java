package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.project.Project;

import javax.swing.tree.DefaultMutableTreeNode;

public class ProjectNode
      extends DefaultMutableTreeNode {
	public ProjectNode(Project proj) {
		setUserObject(proj);
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public boolean getIsLeaf() {
		return false;
	}

	public String toString() {
		return "Project \"" + ((Project) getUserObject()).getName() + "\"";
	}
}
