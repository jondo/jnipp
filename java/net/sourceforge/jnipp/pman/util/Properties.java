package net.sourceforge.jnipp.pman.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Properties {
	private static java.util.Properties props = null;
	private static File jnippProps = null;

	static {
		init();
	}

	private static void init() {
		try {
			String userDir = System.getProperty("user.home");
			if (userDir == null)
				userDir = ".";

			File jnippPath = new File(userDir + File.separatorChar + ".jnipp");
			if (jnippPath.exists() == false)
				jnippPath.mkdirs();
			jnippProps = new File(jnippPath, "jnipp.properties");
			if (jnippProps.exists() == false)
				jnippProps.createNewFile();
			props = new java.util.Properties();
			props.load(new BufferedInputStream(new FileInputStream(jnippProps)));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void setProperty(String propertyName, String propertyValue) {
		props.setProperty(propertyName, propertyValue);
	}

	public static void setProperty(String propertyName, int propertyValue) {
		setProperty(propertyName, new Integer(propertyValue).toString());
	}

	public static String getProperty(String propertyName, String defaultValue) {
		return props.getProperty(propertyName, defaultValue);
	}

	public static int getProperty(String propertyName, int defaultValue) {
		String propertyValue = props.getProperty(propertyName, new Integer(defaultValue).toString());
		return Integer.parseInt(propertyValue);
	}

	public static java.util.Properties getProperties() {
		return props;
	}

	public static void flush() {
		try {
			props.store(new FileOutputStream(jnippProps), "JNI++ Properties");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
