package net.sourceforge.jnipp.pman.util;

import java.io.File;

public class RecentProject {
	private String fullFileName = null;
	private String fileName = null;

	public RecentProject(String fullFileName) {
		this.fullFileName = fullFileName;
		fileName = new File(fullFileName).getName();
	}

	public String getFullFileName() {
		return fullFileName;
	}

	public boolean equals(Object obj) {
		return (((RecentProject) obj).fullFileName.equals(fullFileName));
	}

	public String toString() {
		return fileName;
	}
}
