package net.sourceforge.jnipp.pman.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

public class OutputRedirectorThread
      extends Thread {
	private PipedInputStream in = new PipedInputStream();
	private boolean stop = false;
	private OutputReader outputReader = null;

	public OutputRedirectorThread(OutputReader outputReader) {
		try {
			this.outputReader = outputReader;
			System.setOut(new PrintStream(new PipedOutputStream(in)));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void run() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		stop = false;
		while (stop == false) {
			try {
				while (reader.ready() == true)
					outputReader.output(reader.readLine());

				Thread.sleep(500);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public synchronized void shutDown() {
		stop = true;
	}
}
