package net.sourceforge.jnipp.pman.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

public class RecentProjectList {
	private static TreeMap recentProjectMap = null;
	private static int maxEntries = 12;

	static {
		init();
	}

	private static void init() {
		recentProjectMap = new TreeMap
		      (
		            new Comparator() {
			            public int compare(Object o1, Object o2) {
				            // sort descending
				            return (((Long) o2).compareTo(o1));
			            }

			            public boolean equals(Object obj) {
				            return (obj == this);
			            }
		            }
		      );

		maxEntries = Properties.getProperty("RecentProjects.maxEntries", 12);
		int index = 1;
		String currentEntry = Properties.getProperty("RecentProjects.recent" + index, null);
		for (; index <= maxEntries && currentEntry != null;) {
			add(currentEntry, index);
			currentEntry = Properties.getProperty("RecentProjects.recent" + ++index, null);
		}
	}

	public static Iterator getRecentProjects() {
		Iterator it = recentProjectMap.values().iterator();
		ArrayList recentProjectList = new ArrayList();
		for (int i = 0; i < maxEntries && it.hasNext() == true; ++i)
			recentProjectList.add(it.next());
		return recentProjectList.iterator();
	}

	public static void add(String projectFileName, long offset) {
		RecentProject rp = new RecentProject(projectFileName);
		remove(rp);
		synchronized (recentProjectMap) {
			recentProjectMap.put(new Long(System.currentTimeMillis() + offset), rp);
		}
	}

	public static void add(String projectFileName) {
		add(projectFileName, 0);
	}

	private static void remove(RecentProject rp) {
		synchronized (recentProjectMap) {
			recentProjectMap.values().remove(rp);
		}
	}

	public static void flush() {
		Iterator it = getRecentProjects();
		for (int i = recentProjectMap.size(); it.hasNext() == true; --i)
			Properties.setProperty("RecentProjects.recent" + i, ((RecentProject) it.next()).getFullFileName());
	}
}
