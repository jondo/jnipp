package net.sourceforge.jnipp.pman.util;

public interface OutputReader {
	public void output(String data);
}
