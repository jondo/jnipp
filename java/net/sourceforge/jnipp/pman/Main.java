package net.sourceforge.jnipp.pman;

import net.sourceforge.jnipp.pman.util.OutputReader;
import net.sourceforge.jnipp.pman.util.OutputRedirectorThread;
import net.sourceforge.jnipp.pman.util.Properties;
import net.sourceforge.jnipp.pman.util.RecentProjectList;
import net.sourceforge.jnipp.project.Project;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main {

	private static MainFrame mf = null;
	private static OutputRedirectorThread outputThread = null;

	public static void main(String[] args) {
		try {
			SplashScreen.showSplashScreen();
			Project proj = new Project();
			mf = new MainFrame(proj);
			mf.setLocation(new Point(Properties.getProperty("MainFrame.x", 0), Properties
						.getProperty("MainFrame.y", 0)));
			mf.setSize(new Dimension(Properties.getProperty("MainFrame.width", 400), Properties
						.getProperty("MainFrame.height", 600)));
			// mf.pack();
			mf.setVisible(true);
			mf.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			mf.addWindowListener(new WindowAdapter() {

				public void windowClosed(WindowEvent e) {
					exit();
				}
			});
			outputThread = new OutputRedirectorThread(new OutputReader() {

				public void output(String data) {
					OutputPane.getOutputPane().appendText(data);
				}
			});
			outputThread.start();
			SplashScreen.hideSplashScreen();
			System.out.println("GUI Startup successful.");
		} catch (Exception ex) {
			System.out.println(ex);
			ex.printStackTrace();
		}
	}

	public static void exit() {
		System.out.println("Flushing properties ...");
		RecentProjectList.flush();
		Properties.setProperty("MainFrame.x", mf.getX());
		Properties.setProperty("MainFrame.y", mf.getY());
		Properties.setProperty("MainFrame.width", mf.getWidth());
		Properties.setProperty("MainFrame.height", mf.getHeight());
		Properties.flush();
		System.out.println("Exiting ...");
		if (outputThread != null) {
			try {
				outputThread.shutDown();
				outputThread.join();
			} catch (Exception ex) {
			}
		}
		System.exit(0);
	}
}
