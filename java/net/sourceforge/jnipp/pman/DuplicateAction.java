package net.sourceforge.jnipp.pman;

import javax.swing.*;
import java.awt.event.ActionEvent;

class DuplicateAction
      extends AbstractAction {
	private static DuplicateAction duplicateAction = new DuplicateAction();

	protected static DuplicateAction getDuplicateAction() {
		return duplicateAction;
	}

	public DuplicateAction() {
		super("Duplicate", new ImageIcon(DuplicateAction.class.getResource("/net/sourceforge/jnipp/pman/images/Copy.gif")));
	}

	public void actionPerformed(ActionEvent e) {
	}
}


