package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.HashMap;
import java.util.Iterator;
import net.sourceforge.jnipp.common.DynamicClassLoader.PackageContents;

public class ClassList
      extends JList {
	private DefaultListModel classListModel = new DefaultListModel();
	private HashMap selectedIndexes = new HashMap();
	private EventListenerList classSelectionListeners = new EventListenerList();
	private String packageName = null;

	public ClassList() {
		super();
		setCellRenderer(new ClassListCellRenderer());
		setModel(classListModel);

		getSelectionModel().addListSelectionListener
		      (
		            new ListSelectionListener() {
			            public void valueChanged(ListSelectionEvent e) {
				            if (e.getValueIsAdjusting() == false) {
					            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
					            int first = e.getFirstIndex();
					            int last = e.getLastIndex();
					            for (int i = first; i <= last; ++i) {
						            Integer selectedIndex = new Integer(i);
						            boolean wasSelected = selectedIndexes.containsKey(selectedIndex);
						            if (wasSelected == true && lsm.isSelectedIndex(i) == false) {
							            selectedIndexes.remove(selectedIndex);
							            fireClassSelected((ClassNode) classListModel.getElementAt(i), ClassSelectionEvent.UNSELECTED);
						            } else if (wasSelected == false && lsm.isSelectedIndex(i) == true) {
							            selectedIndexes.put(selectedIndex, classListModel.getElementAt(i));
							            fireClassSelected((ClassNode) classListModel.getElementAt(i), ClassSelectionEvent.SELECTED);
						            }
					            }
				            }
			            }
		            }
		      );
	}

	public synchronized void addClassSelectionListener(ClassSelectionListener l) {
		classSelectionListeners.add(ClassSelectionListener.class, l);
	}

	public synchronized void removeClassSelectionListener(ClassSelectionListener l) {
		classSelectionListeners.remove(ClassSelectionListener.class, l);
	}

	private void fireClassSelected(ClassNode selectedNode, int action) {
		String fullyQualifiedClassName = selectedNode.toString();
		if (packageName != null && packageName.equals("") == false)
			fullyQualifiedClassName = packageName + "." + fullyQualifiedClassName;
		ClassSelectionEvent cse = new ClassSelectionEvent(this, action, fullyQualifiedClassName);

		Object[] listeners = classSelectionListeners.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ClassSelectionListener.class)
				((ClassSelectionListener) listeners[i + 1]).classSelected(cse);
		}
	}

	public void updateClassList(PackageContents pc) {
		packageName = pc.getFullyQualifiedName();
		classListModel.removeAllElements();
		Iterator it = selectedIndexes.values().iterator();
		while (it.hasNext() == true)
			fireClassSelected((ClassNode) it.next(), ClassSelectionEvent.UNSELECTED);
		selectedIndexes.clear();
		it = pc.getContainedClasses().values().iterator();
		while (it.hasNext() == true) {
			String shortName = (String) it.next();
			String className = shortName;
			if (packageName != null && packageName.equals("") == false)
				className = packageName + "." + shortName;

			try {
				classListModel.addElement(new ClassNode(shortName, Class.forName(className)));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
