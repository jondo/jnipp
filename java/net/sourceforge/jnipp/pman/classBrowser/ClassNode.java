package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.tree.DefaultMutableTreeNode;

public class ClassNode
      extends DefaultMutableTreeNode {
	private String shortName = null;

	public ClassNode(String shortName, Class cls) {
		this.shortName = shortName;
		setUserObject(cls);
	}

	public boolean getAllowsChildren() {
		return false;
	}

	public boolean getIsLeaf() {
		return false;
	}

	public boolean isInterface() {
		return ((Class) getUserObject()).isInterface();
	}

	public String toString() {
		return shortName;
	}
}
