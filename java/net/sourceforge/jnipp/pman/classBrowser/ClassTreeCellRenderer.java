package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class ClassTreeCellRenderer
      extends DefaultTreeCellRenderer {
	public Component getTreeCellRendererComponent(JTree tree,
	                                              Object value,
	                                              boolean selected,
	                                              boolean expanded,
	                                              boolean leaf,
	                                              int row,
	                                              boolean hasFocus) {
		Component comp = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/" + (expanded == true ? "open" : "closed") + "Folder.gif")));
		return comp;
	}

}
