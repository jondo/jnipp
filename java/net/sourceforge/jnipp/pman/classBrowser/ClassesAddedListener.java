package net.sourceforge.jnipp.pman.classBrowser;

import java.util.EventListener;

public interface ClassesAddedListener
      extends EventListener {
	public void classesAdded(ClassesAddedEvent e);
}
