package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClassSelectionDialog
      extends JDialog {
	private ClassBrowserPanel browserPanel = new ClassBrowserPanel();
	private ArrayList selectedClassList = new ArrayList();
	private EventListenerList classesAddedListeners = new EventListenerList();

	public ClassSelectionDialog(JFrame owner, String title) {
		super(owner, title, true);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		JButton addButton = new JButton("Add");
		JButton closeButton = new JButton("Close");
		buttonPanel.add(addButton);
		buttonPanel.add(closeButton);
		mainPanel.add(browserPanel, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		getContentPane().add(mainPanel);

		browserPanel.addClassSelectionListener
		      (
		            new ClassSelectionListener() {
			            public void classSelected(ClassSelectionEvent e) {
				            if (e.getAction() == ClassSelectionEvent.SELECTED)
					            selectedClassList.add(e.getClassName());
				            else
					            selectedClassList.remove(e.getClassName());
			            }
		            }
		      );

		addButton.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            fireClassesAdded();
			            }
		            }
		      );

		closeButton.addActionListener
		      (
		            new ActionListener() {
			            public void actionPerformed(ActionEvent e) {
				            hide();
			            }
		            }
		      );
	}

	public void addClassesAddedListener(ClassesAddedListener l) {
		classesAddedListeners.add(ClassesAddedListener.class, l);
	}

	public void removeClassesAddedListener(ClassesAddedListener l) {
		classesAddedListeners.remove(ClassesAddedListener.class, l);
	}

	private void fireClassesAdded() {
		ArrayList classList = (ArrayList) selectedClassList.clone();
		ClassesAddedEvent cae = new ClassesAddedEvent(this, classList);
		Object[] listeners = classesAddedListeners.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ClassesAddedListener.class)
				((ClassesAddedListener) listeners[i + 1]).classesAdded(cae);
		}

		selectedClassList.clear();
	}
}
