package net.sourceforge.jnipp.pman.classBrowser;

import java.util.EventListener;

public interface ClassSelectionListener
      extends EventListener {
	public void classSelected(ClassSelectionEvent e);
}
