package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.*;
import java.awt.*;

public class ClassListCellRenderer
      extends DefaultListCellRenderer {
	public Component getListCellRendererComponent(JList list,
	                                              Object value,
	                                              int index,
	                                              boolean isSelected,
	                                              boolean cellHasFocus) {
		Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

		ClassNode node = (ClassNode) value;
		Class cls = null;

		if (node.isInterface() == true)
			setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/interface.gif")));
		else
			setIcon(new ImageIcon(getClass().getResource("/net/sourceforge/jnipp/pman/images/class.gif")));

		return comp;
	}
}
