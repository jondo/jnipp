package net.sourceforge.jnipp.pman.classBrowser;

import javax.swing.tree.DefaultMutableTreeNode;
import net.sourceforge.jnipp.common.DynamicClassLoader.PackageContents;

public class PackageNode
      extends DefaultMutableTreeNode {
	public PackageNode(PackageContents pc) {
		setUserObject(pc);
	}

	public boolean getAllowsChildren() {
		return true;
	}

	public boolean getIsLeaf() {
		return false;
	}

	public String toString() {
		return ((PackageContents) getUserObject()).getName();
	}
}
