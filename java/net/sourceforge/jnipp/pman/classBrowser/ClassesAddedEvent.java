package net.sourceforge.jnipp.pman.classBrowser;

import java.awt.*;
import java.util.Collection;
import java.util.Iterator;

public class ClassesAddedEvent
      extends AWTEvent {
	public final static int ADDED = AWTEvent.RESERVED_ID_MAX + 1;
	private Collection classNames = null;

	public ClassesAddedEvent(ClassSelectionDialog source, Collection classNames) {
		super(source, ADDED);
		this.classNames = classNames;
	}

	public Iterator getClassNames() {
		return classNames.iterator();
	}
}
