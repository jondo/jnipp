package net.sourceforge.jnipp.pman.classBrowser;

import java.awt.*;

public class ClassSelectionEvent
      extends AWTEvent {
	public final static int SELECTED = AWTEvent.RESERVED_ID_MAX + 1;
	public final static int UNSELECTED = AWTEvent.RESERVED_ID_MAX + 2;

	private String className = null;
	private int action = 0;

	public ClassSelectionEvent(ClassList source, int action, String className) {
		super(source, action);
		this.className = className;
		this.action = action;
	}

	public String getClassName() {
		return className;
	}

	public int getAction() {
		return action;
	}
}
