package net.sourceforge.jnipp.pman.classBrowser;

import net.sourceforge.jnipp.common.DynamicClassLoader;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.Iterator;
import net.sourceforge.jnipp.common.DynamicClassLoader.PackageContents;

public class ClassBrowserPanel
      extends JPanel {
	private JTree classTree = null;
	private DefaultTreeModel classTreeModel = null;
	private ClassList classList = new ClassList();
	private DynamicClassLoader classLoader = new DynamicClassLoader();
	private PackageContents currentPackage = null;

	public ClassBrowserPanel() {
		super();
		setLayout(new BorderLayout());

		createTreeModel(classLoader.getLoadablePackages());
		classTree = new JTree(classTreeModel);
		classTree.setCellRenderer(new ClassTreeCellRenderer());
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(classTree), new JScrollPane(classList));
		splitPane.setResizeWeight(.9);
		splitPane.setOneTouchExpandable(true);
		add(splitPane, BorderLayout.CENTER);

		classTree.addTreeSelectionListener
		      (
		            new TreeSelectionListener() {
			            public void valueChanged(TreeSelectionEvent e) {
				            TreePath path = e.getNewLeadSelectionPath();
				            if (path != null) {
					            DefaultMutableTreeNode selected = (DefaultMutableTreeNode) path.getLastPathComponent();
					            if (selected instanceof PackageNode) {
						            currentPackage = (PackageContents) selected.getUserObject();
						            classList.updateClassList(currentPackage);
					            }
				            }
			            }
		            }
		      );
	}

	private void createTreeModel(PackageContents pc) {
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("All Packages");
		Iterator it = classLoader.getLoadablePackages().getContainedPackages().values().iterator();
		while (it.hasNext() == true)
			rootNode.add(createBranch((PackageContents) it.next()));
		classTreeModel = new DefaultTreeModel(rootNode);
		classTreeModel.setAsksAllowsChildren(true);
	}

	private PackageNode createBranch(PackageContents pc) {
		PackageNode thisBranch = new PackageNode(pc);
		Iterator it = pc.getContainedPackages().values().iterator();
		while (it.hasNext() == true)
			thisBranch.add(createBranch((PackageContents) it.next()));
		return thisBranch;
	}

	public void addClassSelectionListener(ClassSelectionListener l) {
		classList.addClassSelectionListener(l);
	}
}
