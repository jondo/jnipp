package net.sourceforge.jnipp.project;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class MacMakefileSettings
      implements Cloneable {
	private String name = null;
	private Project project = null;

	public MacMakefileSettings(Project project, Element elementNode)
	      throws DOMException, ProjectFormatException {
		this.project = project;
		initialize(elementNode);
	}

	public MacMakefileSettings(Project project) {
		this.project = project;
	}

	public MacMakefileSettings(Project project, String name) {
		this.project = project;
		setName(name);
	}

	private void initialize(Element elementNode)
	      throws DOMException, ProjectFormatException {
		if (elementNode.hasAttribute("name") == false)
			throw new ProjectFormatException("attribute \"name\" required for \"macmakefile\" element");
		name = elementNode.getAttribute("name");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public Node getDOMNode(Document targetDoc) {
		Element node = targetDoc.createElement("macmakefile");
		node.setAttribute("name", name);

		return node;
	}

	public Object clone() {
		MacMakefileSettings copy = new MacMakefileSettings(project);
		copy.name = name;
		return copy;
	}

	public boolean equals(Object rhs) {
		if (rhs == null || (rhs instanceof MacMakefileSettings) == false)
			return false;

		return ((MacMakefileSettings) rhs).name.equals(name);
	}
}
