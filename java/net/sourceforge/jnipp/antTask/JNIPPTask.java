package net.sourceforge.jnipp.antTask;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.types.CommandlineJava;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Reference;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author $Author$
 * @version $Revision$
 */
public class JNIPPTask
      extends Task {
	private CommandlineJava cmdl = new CommandlineJava();
	private String projectFile = null;
	private File dir = null;

	public void setProjectFile(String projectFile) {
		this.projectFile = projectFile;
	}

	public void setDir(File dir) {
		this.dir = dir;
	}

	public Path createClasspath() {
		return cmdl.createClasspath(project).createPath();
	}

	public void setClasspath(Path classPath) {
		createClasspath().append(classPath);
	}

	public void setClasspathRef(Reference r) {
		createClasspath().setRefid(r);
	}

	public void execute()
	      throws BuildException {
		if (projectFile == null)
			throw new BuildException("projectFile must be specified", location);

		if (dir == null)
			dir = project.getBaseDir();
		else if (dir.exists() == false || dir.isDirectory() == false)
			throw new BuildException(dir.getAbsolutePath() + " is not a valid directory", location);

		cmdl.setVm("java");
		cmdl.setClassname("net.sourceforge.jnipp.main.Main");
		cmdl.createArgument().setValue("-projectFile=" + projectFile);

		Execute exe = new Execute();
		exe.setAntRun(project);
		exe.setWorkingDirectory(dir);
		exe.setCommandline(cmdl.getCommandline());
		try {
			exe.execute();
		} catch (IOException ex) {
			throw new BuildException(ex);
		}
	}
}